/*
 * Copyright (C) 2007, 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _GOFIND_FILTER_H
#define _GOFIND_FILTER_H

#include "taghandler.h"

#include <set>
#include <iostream>
#include <ept/debtags/tag.h>

#include <boolstuff/BoolExpr.h>

class PackageFilter
{
public:
	PackageFilter();
	~PackageFilter();

	inline void Clean() { DeleteList(); }
	bool Load(const char *filename);
	bool Load(FILE *filedesc);

	typedef enum {
		Unknown = 1,   // The calification for the tag/package is unknown
		Recommended,   // The tag/package is recommended
		Safe,          // Green light, the tag/package is safe
		Normal,        // Normal package, nothing special about it
		Warning,       // Yellow light, handle with care
		Dangerous,     // Red light, the tag/package is definitely unsafe
		Intolerable,   // Mayday, mayday, the tag/package might be really dangerous!
		Highlight,     // Especially remarkable for some reason
		Hidden         // Invisible, hidden, not shown
	} Type;

	static const char *const TypeNames[16];
	inline static const char * TypeName(int i) { return TypeNames[i]; }

	typedef ept::debtags::Tag Tag;
	typedef std::set<Tag> TagSet;

	int TagValue(const Tag &tag);
	int TagsValue(const TagSet &tags);

protected:
	class ResultList
	{
		public:
			inline ResultList(const std::string str, Type t, int age) {
				name = str;
				type = t;
				minimal_age = age;
				next= NULL;
			}

			inline PackageFilter::ResultList *GetLast() {
				PackageFilter::ResultList *l = this, *t;
				while ( (t = l->next) != NULL) l = t;
				return l;
			}

			std::string name;
			Type type;
			int minimal_age;
			FilterTagHandler::Result positive;
			FilterTagHandler::Result negative;
			PackageFilter::ResultList *next;
	};

	FilterTagHandler tagdata;
	PackageFilter::ResultList *list;

	inline PackageFilter::ResultList *GetFirst() {
		return list;
	}

	inline PackageFilter::ResultList *GetLast() {
		return list ? list->GetLast() : NULL;
	}

	inline void AddFirst(PackageFilter::ResultList *new_list) {
		if (!new_list) return;
		PackageFilter::ResultList *old_list = list;
		list = new_list;
		list->GetLast()->next = old_list;
	}

	bool AddLastAndHelper(ResultList *element, boolstuff::BoolExpr<std::string> *expr);
	bool AddLastOrHelper(const std::string str, Type type, int age, boolstuff::BoolExpr<std::string> *expr);
	bool AddLast(const std::string str, Type type, int age, const std::string &bool_expr);

	inline void AddLast(PackageFilter::ResultList *new_list) {
		if (!list) { list = new_list; }
		else { list->GetLast()->next = new_list; }
	}

	inline void DeleteList() {
		while (list) {
			PackageFilter::ResultList *rest = list->next;
			list->next = NULL;
			delete list;
			list = rest;
		}
	}

	public:
		inline void Print(std::ostream &out) const {
			PackageFilter::ResultList *item = list;
			while (item) {
				out << item->name << ": CLASS=" << PackageFilter::TypeName(item->type) << " (" << item->type
					<< "), AGE=" << item->minimal_age << std::endl;
				out << " +: ";
				tagdata.Print(out, &item->positive);
				out << " -: ";
				tagdata.Print(out, &item->negative);
				item = item->next;
			}
		}
};

inline std::ostream &operator << (std::ostream &out, const PackageFilter *filter)
{
	if (filter != NULL)
		filter->Print(out);
	return out;
}

#endif // _GOFIND_FILTER_H
