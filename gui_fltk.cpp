/*
 * debtags - Implement package tags support for Debian
 *
 * Copyright (C) 2007  Enrico Zini <enrico@debian.org>
 * Copyright (C) 2007, 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "common.h"
#include "pkgdata.h"
#include "fltk/ui.h"

#include <FL/Fl.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/fl_ask.H>

#ifdef USE_GETTEXT
#include <libintl.h>
#include <locale.h>
#define _(String) gettext (String)
#else
// Work-around until goplay is actually gettextised
#define gettext(String) (String)
#define _(String) (String)
#endif

#include <vector>
#include <math.h>

#include <ept/apt/packagerecord.h>
#include <ept/textsearch/textsearch.h>

static const char* ReadFlChoice(Fl_Choice& c)
{
	const Fl_Menu_Item* cur = c.mvalue();
	if (cur->user_data_)
		return (const char*)cur->user_data_;
	else
		return "";
}

static void UpdateUILists(GamesUI& ui)
{
	PackageData &pkgdata = *(PackageData *)(ui.user_data);

	// Defining this here, non-const, because fltk's interface want non-const
	// strings and I did not have time to check whether it would attempt to
	// modify the parameter or not.  Likely not, and thus a
	// const_cast<char*>(VoidString) would probably have been better.
	// --Enrico
	char VoidString[1] = "";
	const char* oldType = ReadFlChoice(*ui.TypeSelection);
	const char* oldIface = ReadFlChoice(*ui.InterfaceSelection);
	ui.TypeSelection->clear();
	ui.TypeSelection->add(_("Any type"), 0, NULL, VoidString, FL_NORMAL_LABEL);
	ui.InterfaceSelection->clear();
	ui.InterfaceSelection->add(_("Any interface"), 0, NULL, VoidString, FL_NORMAL_LABEL);

	static int widths_with_popcon[] = { 100, 300, 0 }; // widths for each column
	static int widths_without_popcon[] = { 100, 0 };
	ui.ResultsBrowser->clear();
	if (pkgdata.popcon().hasData())
	{
		ui.ResultsBrowser->column_widths(widths_with_popcon);
		// tab delimited columns with colors
		ui.ResultsBrowser->add(_("@B12@C7@b@.PACKAGE\t@B12@C7@b@.DESCRIPTION\t@B12@C7@b@.POPCON"));
	}
	else
	{
		ui.ResultsBrowser->column_widths(widths_without_popcon);
		// tab delimited columns with colors
		ui.ResultsBrowser->add(_("@B12@C7@b@.PACKAGE\t@B12@C7@b@.DESCRIPTION"));
	}

	// FIXME: there are better ways to remember the previous item

	const PackageData::TagSet types = pkgdata.types();
	int newIdx = 0;
	for (PackageData::TagSet::const_iterator i = types.begin();
			i != types.end(); ++i)
	{
		int idx = ui.TypeSelection->add(gettext(i->shortDescription().c_str()),
							0, NULL, pkgdata.TagString(*i), FL_NORMAL_LABEL);
		if (i->fullname() == oldType)
			newIdx = idx;
	}
	ui.TypeSelection->value(newIdx);

	const PackageData::TagSet ifaces = pkgdata.interfaces();
	newIdx = 0;
	for (PackageData::TagSet::const_iterator i = ifaces.begin();
			i != ifaces.end(); ++i)
	{
		int idx = ui.InterfaceSelection->add(gettext(i->shortDescription().c_str()),
							0, NULL, pkgdata.TagString(*i), FL_NORMAL_LABEL);
		if (i->fullname() == oldIface)
			newIdx = idx;
	}
	ui.InterfaceSelection->value(newIdx);

	const std::vector<Result> res = pkgdata.results();
	for (std::vector<Result>::const_iterator i = res.begin();
			i != res.end(); ++i)
	{
		ept::apt::PackageRecord rec(pkgdata.apt().rawRecord(i->name));
		char* userData = pkgdata.PackageString(rec.package());
		bool showPkg = true;

		// Available Colors: FL_BLACK, FL_BLUE, FL_CYAN, FL_DARK_BLUE,
		// FL_DARK_CYAN, FL_DARK_GREEN FL_DARK_MAGENTA, FL_DARK_RED,
		// FL_DARK_YELLOW, FL_GREEN, FL_MAGENTA, FL_RED, FL_WHITE, FL_YELLOW

		Fl_Color bk(FL_WHITE);
		Fl_Color fr(FL_BLACK);
		PackageData::TagSet tags = pkgdata.debtags().getTagsOfItem((const char *)rec.package().c_str());
		switch (pkgdata.GetPackageFilter().TagsValue(tags))
		{
			case PackageFilter::Hidden:
				fr = FL_CYAN; bk = FL_WHITE;
				showPkg = false;
				break;
			case PackageFilter::Highlight:
				fr = FL_WHITE; bk = FL_DARK_MAGENTA; break;
			case PackageFilter::Intolerable:
				fr = FL_WHITE; bk = FL_BLACK; break;
			case PackageFilter::Dangerous:
				fr = FL_WHITE; bk = FL_RED; break;
			case PackageFilter::Warning:
				fr = FL_BLACK; bk = FL_YELLOW; break;
			case PackageFilter::Safe:
				fr = FL_BLACK; bk = FL_GREEN; break;
			case PackageFilter::Recommended:
				fr = FL_BLACK; bk = FL_BLUE; break;
			case PackageFilter::Normal:
			default:
				fr = FL_BLACK; bk = FL_WHITE; break;
		}

		if (showPkg)
		{
			char fmtstr[16];
			snprintf(fmtstr, sizeof(fmtstr), "@B%d@C%d@.", bk, fr);

			std::string desc;
			std::string shortdesc;
			if (pkgdata.GetPkgLatin1ShortDesc(rec.package(), shortdesc))
			{
				desc = std::string(fmtstr) + rec.package() + "\t" + 
					std::string(fmtstr) + shortdesc;
			} else {
				desc = std::string(fmtstr) + rec.package() + "\t" + 
					std::string(fmtstr) + rec.shortDescription();
			}

			if (pkgdata.popcon().hasData() && i->popcon)
			{
				desc += "\t" + std::string(fmtstr);
				char stars[16];
				snprintf(stars, sizeof(stars), "%%%d/8/1;",
					(int)rintf(log(i->popcon) * 100 / log(pkgdata.popconLocalMax())));
				desc += stars;
				//printf ("%s (%s): POPCON=%f\n", rec.package().c_str(), rec.shortDescription().c_str(), i->popcon);
			}
			ui.ResultsBrowser->add(desc.c_str(), userData);
		}

		// Relevance is 0 to 100
		// Popcon is a weird floating point number (to be improved)
		//FIXMEaddToResults(rec.package() + " - " + rec.shortDescription(), i->relevance, i->popcon);
	}
}

static void CallBackTypeSelection(Fl_Choice* choice, void *data)
{
	//printf("CallBackTypeSelection\n");
	//fflush(stdout);
	GamesUI& ui = *static_cast<GamesUI*>(data);
	PackageData &pkgdata = *(PackageData *)(ui.user_data);
	PackageData::Tag tag = pkgdata.voc().tagByName(ReadFlChoice(*choice));
	pkgdata.setTypeFilter(tag);
	UpdateUILists(ui);
}

static void CallBackInterfaceSelection(Fl_Choice* choice, void *data)
{
	//printf("CallBackInterfaceSelection\n");
	//fflush(stdout);
	GamesUI& ui = *static_cast<GamesUI*>(data);
	PackageData &pkgdata = *(PackageData *)(ui.user_data);
	PackageData::Tag tag = pkgdata.voc().tagByName(ReadFlChoice(*choice));
	pkgdata.setInterfaceFilter(tag);
	UpdateUILists(ui);
}

static void CallBackSearchInput(Fl_Input* input, void *data)
{
	//printf("CallBackSearchInput\n"); fflush(stdout);
	GamesUI& ui = *static_cast<GamesUI*>(data);
	PackageData &pkgdata = *(PackageData *)(ui.user_data);
	pkgdata.setKeywordFilter(input->value());
	UpdateUILists(ui);
}

static void CallBackAlreadyInstalled(Fl_Round_Button*, void *data)
{
	//printf("CallBackInstalled\n"); fflush(stdout);
	GamesUI& ui = *static_cast<GamesUI*>(data);
	PackageData &pkgdata = *(PackageData *)(ui.user_data);
	ui.AlreadyInstalled->value(1); ui.AlreadyInstalled->redraw();
	ui.ToBeInstalled->value(0); ui.ToBeInstalled->redraw();
	ui.InstalledOrNot->value(0); ui.InstalledOrNot->redraw();
	pkgdata.setInstalledFilter(Engine::INSTALLED);
	UpdateUILists(ui);
}

static void CallBackToBeInstalled(Fl_Round_Button*, void *data)
{
	//printf("CallBackToBeInstalled\n"); fflush(stdout);
	GamesUI& ui = *static_cast<GamesUI*>(data);
	PackageData &pkgdata = *(PackageData *)(ui.user_data);
	ui.AlreadyInstalled->value(0); ui.AlreadyInstalled->redraw();
	ui.ToBeInstalled->value(1); ui.ToBeInstalled->redraw();
	ui.InstalledOrNot->value(0); ui.InstalledOrNot->redraw();
	pkgdata.setInstalledFilter(Engine::NOTINSTALLED);
	UpdateUILists(ui);
}

static void CallBackInstalledOrNot(Fl_Round_Button*, void *data)
{
	//printf("CallBackToBeInstalled\n"); fflush(stdout);
	GamesUI& ui = *static_cast<GamesUI*>(data);
	PackageData &pkgdata = *(PackageData *)(ui.user_data);
	ui.AlreadyInstalled->value(0); ui.AlreadyInstalled->redraw();
	ui.ToBeInstalled->value(0); ui.ToBeInstalled->redraw();
	ui.InstalledOrNot->value(1); ui.InstalledOrNot->redraw();
	pkgdata.setInstalledFilter(Engine::ANY);
	UpdateUILists(ui);
}

static bool Go(PackageData &pkgdata)
{
#ifdef USE_GETTEXT
	setlocale (LC_MESSAGES, "");
	setlocale (LC_CTYPE, "");
	setlocale (LC_COLLATE, "");
	textdomain ("gofind");
	bindtextdomain ("gofind", NULL);
#endif

	GamesUI mainui;
	mainui.user_data = &pkgdata;
	Fl_Double_Window *window = mainui.CreateWindows();
	mainui.TypeSelection->callback((Fl_Callback*)CallBackTypeSelection, &mainui);
	mainui.TypeSelection->when(FL_WHEN_CHANGED);
	mainui.InterfaceSelection->callback((Fl_Callback*)CallBackInterfaceSelection, &mainui);
	mainui.InterfaceSelection->when(FL_WHEN_CHANGED);
	mainui.SearchInput->callback((Fl_Callback*)CallBackSearchInput, &mainui);
	mainui.SearchInput->when(FL_WHEN_CHANGED);
	mainui.AlreadyInstalled->callback((Fl_Callback*)CallBackAlreadyInstalled, &mainui);
	mainui.AlreadyInstalled->when(FL_WHEN_CHANGED);
	mainui.ToBeInstalled->callback((Fl_Callback*)CallBackToBeInstalled, &mainui);
	mainui.ToBeInstalled->when(FL_WHEN_CHANGED);
	mainui.InstalledOrNot->callback((Fl_Callback*)CallBackInstalledOrNot, &mainui);
	mainui.InstalledOrNot->when(FL_WHEN_CHANGED);

	mainui.AlreadyInstalled->value(0); mainui.AlreadyInstalled->redraw();
	mainui.ToBeInstalled->value(0); mainui.ToBeInstalled->redraw();
	mainui.InstalledOrNot->value(1); mainui.InstalledOrNot->redraw();
//	mainui.engine->setInstalledFilter(Engine::ANY);

	mainui.Screenshot(new Fl_PNG_Image(FILE_NO_SCREENSHOT));
	mainui.AboutView->load(HTMLDIR "/about.en.html");

	UpdateUILists(mainui);

	int argc = 1;
	const char *argv[32] = { "GoFind!", NULL };

	window->show(argc, (char**)argv);
	while (Fl::wait());
	return 0;
}


// Exported Stuff

extern "C" int init(int argc, const char *argv[])
{
	std::cout << _("FLTK Plugin successfully loaded") << std::endl;
	return 1; // true
}

extern "C" void comment(const char *text)
{
	std::cout << text << std::endl;
}

extern "C" int go(PackageData &pkgdata)
{
	return Go(pkgdata);
}
