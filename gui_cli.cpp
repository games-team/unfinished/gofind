/*
 * Copyright (C) 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "common.h"

#include "filter.h"
#include "slre.h"
#include "pkgdata.h"

#include <typeinfo>
#include <iostream>
#include <string>
#include <string.h>

#include <ept/apt/packagerecord.h>
#include <ept/textsearch/textsearch.h>
#include <wibble/regexp.h>
#include <wibble/string.h>
#include <xapian.h>

#include <iostream>
#include <cmath>

#include <limits.h>
#include <sys/stat.h>

#ifdef USE_GETTEXT
#include <libintl.h>
#include <locale.h>
#define _(String) gettext (String)
#else
// Work-around until goplay is actually gettextised
#define gettext(String) (String)
#define _(String) (String)
#endif

using namespace std;
using namespace ept;
using namespace ept::debtags;
using namespace ept::apt;
using namespace ept::textsearch;

static bool Go(PackageData &pkgdata)
{
	std::ostream &out = std::cout;

	bool run = true;

	out << "# System running" << std::endl;

	struct slre_pattern pattern[16];
	const char *pattern_text[16];
	char pattern_type[16];
	int i=0;
	pattern_type[i]='H'; pattern_text[i++] = "^\\s*(HELP|H)\\s*$";
	pattern_type[i]='T'; pattern_text[i++] = "^\\s*(TY|TYPES)\\s*$";
	pattern_type[i]='I'; pattern_text[i++] = "^\\s*(I|INTERFACES)\\s*$";
	pattern_type[i]='R'; pattern_text[i++] = "^\\s*(R|RESULTS)\\s*$";
	pattern_type[i]='P'; pattern_text[i++] = "^\\s*(P|PACKAGE)\\s+(\\S+)\\s*$";
	pattern_type[i]='S'; pattern_text[i++] = "^\\s*(S|SCREENSHOT)\\s+(\\S+)\\s*$";
	pattern_type[i]='X'; pattern_text[i++] = "^\\s*(TA|TAGS)\\s+(\\S+)\\s*$";

	pattern_type[i]='g'; pattern_text[i++] = "^\\s*(GET|G)\\s+(\\S+)\\s*$";
	pattern_type[i]='s'; pattern_text[i++] = "^\\s*(SET|S)\\s+(\\S+)\\s*=\\s*((\\S|\\s)*\\S)\\s*$";

	pattern_type[i]='\0'; pattern_text[i++] = NULL;

	int j = 0;
	while (pattern_text[j] != NULL)
	{
		if (!slre_compile(&pattern[j], pattern_text[j]))
			std::cerr << _("Error compiling RE: ") << pattern[j].err_str << std::endl;
		j++;
	}

	while (run && !feof(stdin))
	{
		char buffer[128];
		struct slre_capture captures[16];
		fgets(buffer, sizeof(buffer), stdin);
		if (*buffer == '#') continue;
		for (char *pnt = buffer; *pnt; pnt++)
		{
			if (*pnt=='\0' || *pnt=='\r' || *pnt=='\n')
			{
				*pnt = '\0';
				break;
			}
		}

		static const char *pattern_name[16] = {
			"Show help",
			"List types",
			"List interfaces",
			"List results",
			"Show package",
			"Show screenshot",
			"Show tags",
			NULL
		};

		int match = -1;
		int j = 0;
		while (match == -1 && pattern_text[j] != NULL && pattern_name[j] != NULL)
		{
			if (slre_match(pattern+j, buffer, strlen(buffer), captures))
			{
				match = j;
				out << "# Command: " << pattern_name[match] << std::endl;
			}
			j++;
		}

		if (match == -1)
			out << "# Syntax Error: Command pattern not recognized" << std::endl;
		else switch (pattern_type[match])
		{
			case 'H':
			{
				out << "# Commands:" << std::endl
					<< "# HELP" << std::endl
					<< "# TYPES" << std::endl
					<< "# INTERFACES" << std::endl
					<< "# RESULTS" << std::endl
					<< "# LIST TYPES|INTERFACES|RESULTS" << std::endl
					<< "# GET <param>" << std::endl
					<< "# SET <param>=<value>" << std::endl;
			}
			break;

			case 'T': case 't':
			{
				const PackageData::TagSet types = pkgdata.types();
				for (PackageData::TagSet::const_iterator i = types.begin(); i != types.end(); ++i)
				{
					out << " " << i->fullname() << "\t" << i->shortDescription().c_str() << std::endl;
				}
				out << std::endl ;
			}
			break;

			case 'I': case 'i':
			{
				const PackageData::TagSet ifaces = pkgdata.interfaces();
				for (PackageData::TagSet::const_iterator i = ifaces.begin();
						i != ifaces.end(); ++i)
				{
					out << " " << i->fullname() << "\t" << i->shortDescription().c_str() << std::endl;
				}
				out << std::endl ;
			}
			break;

			case 'R': case 'r':
			{
				const std::vector<Result> res = pkgdata.results();
				for (vector<Result>::const_iterator i = res.begin();
						i != res.end(); ++i)
				{
					PackageRecord rec(pkgdata.apt().rawRecord(i->name));
//					char* userData = pkgString(rec.package());
						set<Tag> tags = pkgdata.debtags().getTagsOfItem((const char *)rec.package().c_str());
					switch (pkgdata.GetPackageFilter().TagsValue(tags))
					{
						default:
							break;
					}

					out << " "<<  rec.package() + "\t" + rec.shortDescription();

					// Relevance is 0 to 100
					// Popcon is a weird floating point number (to be improved)
					//FIXMEaddToResults(rec.package() + " - " + rec.shortDescription(), i->relevance, i->popcon);
					if (pkgdata.popcon().hasData() && i->popcon)
					{
						out << "\tPOPCON=" << (int)rintf(log(i->popcon) * 100 / log(pkgdata.popconLocalMax()));
					}

					if (pkgdata.popcon().hasData() && i->relevance)
					{
						out << "\tRELEVANCE=" << i->relevance;
					}

					out << std::endl ;
				}

				out << std::endl ;
			}
			break;

			case 'P':
			{
				char name[sizeof(buffer)];
				strncpy(name, captures[2].ptr, captures[2].len);
				name[captures[2].len] = '\0';
				out << "# Package " << name << std::endl ;

				std::string pkg_desc;

				PackageRecord rec(pkgdata.apt().rawRecord(name));
				out << "NAME=" << rec.package() << std::endl ;
				out << "DESC=" << rec.shortDescription() << std::endl ;
				out << "LONG=" << rec.longDescription() << std::endl ;

				out << std::endl ;
			}
			break;

			case 'S':
			{
				char name[sizeof(buffer)];
				strncpy(name, captures[2].ptr, captures[2].len);
				name[captures[2].len] = '\0';
				out << "# Package " << name << std::endl ;

				char filename[PATH_MAX];
				struct stat fileinfo;

				if (snprintf(filename, PATH_MAX, "%s/%s.png", THUMBNAILSDIR, name) &&
					stat(filename, &fileinfo) == 0)
				{ // Portable Network Graphics (PNG) image files
					// We were able to get the file attributes so the file obviously exists.
					out << "PNG=" << filename << std::endl ;
				}
				else if (snprintf(filename, PATH_MAX, "%s/%s.jpg", THUMBNAILSDIR, name) &&
					stat(filename, &fileinfo) == 0)
				{ // Joint Photographic Experts Group (JPEG) File Interchange Format (JFIF) images.
					out << "JPEG=" << filename << std::endl ;
				}
				else if (snprintf(filename, PATH_MAX, "%s/%s.jpeg", THUMBNAILSDIR, name) &&
					stat(filename, &fileinfo) == 0)
				{
					out << "JPEG=" << filename << std::endl ;
				}
				else if (snprintf(filename, PATH_MAX, "%s/%s.bmp", THUMBNAILSDIR, name) &&
					stat(filename, &fileinfo) == 0)
				{ // Windows Bitmap (BMP) image files
					out << "BMP=" << filename << std::endl ;
				}
				else
				{ // TODO: Portable Anymap (PNM, PBM, PGM, PPM) image files
					out << "NULL" << std::endl ;
				}

				out << std::endl ;
			}
			break;

			case 'X':
			{
				char name[sizeof(buffer)];
				strncpy(name, captures[2].ptr, captures[2].len);
				name[captures[2].len] = '\0';
				out << "# Package " << name << std::endl ;

				ept::debtags::Debtags debtags;
				set<ept::debtags::Tag> tags = debtags.getTagsOfItem(name);
				for (set<ept::debtags::Tag>::const_iterator i = tags.begin(); i != tags.end(); ++i)
				{
					out
						<< i->facet().name() << "::" << i->name() << " \""
						<< gettext(i->facet().shortDescription().c_str())
						<< "\"::\"" << gettext(i->shortDescription().c_str()) << "\""
						<< std::endl ;
				}

				out << std::endl ;
			}
			break;

			case 'g':
			{
				char name[sizeof(buffer)];
				strncpy(name, captures[2].ptr, captures[2].len);
				name[captures[2].len] = '\0';
				out << "# Get " << name << std::endl ;
				if (slre_compare_capture_multi(&captures[2], 2, "INSTALLED", "INST"))
				{
					switch (pkgdata.getInstalledFilter())
					{
						case Engine::INSTALLED:
							out << "installed=yes" << std::endl ;
							break;
						case Engine::NOTINSTALLED:
							out << "installed=no" << std::endl ;
							break;
						case Engine::ANY:
							out << "installed=any" << std::endl ;
							break;
						default:
							out << "#unknown installed value" << std::endl ;
							break;
					}
				}
				else if (slre_compare_capture_multi(&captures[2], 4, "KEYWORD", "KEYW", "KW", "K"))
				{
					out << "keyword=\"" << pkgdata.getKeywordFilter() << "\"" << std::endl ;
				}
				else if (slre_compare_capture_multi(&captures[2], 2, "TYPE", "T"))
				{
					out << "type=?" << std::endl ;
				}
				else if (slre_compare_capture_multi(&captures[2], 3, "INTERFACE", "IFACE", "IF"))
				{
					out << "interface=?" << std::endl ;
				}
				else
				{
					out << "# Unknown parameter name" << std::endl ;
				}
			}
			break;

			case 's':
			{
				char name[sizeof(buffer)];
				strncpy(name, captures[2].ptr, captures[2].len);
				name[captures[2].len] = '\0';
				out << "# Set " << name << " to " << captures[3].ptr <<std::endl ;
				if (slre_compare_capture_multi(&captures[2], 2, "INSTALLED", "INST"))
				{
					if (slre_compare_capture_multi(&captures[3], 4, "yes", "y", "true", "1"))
					{
						out << "# installed=yes" << std::endl ;
						pkgdata.setInstalledFilter(Engine::INSTALLED);
					}
					else if (slre_compare_capture_multi(&captures[3], 4, "no", "n", "false", "0"))
					{
						out << "# installed=no" << std::endl ;
						pkgdata.setInstalledFilter(Engine::NOTINSTALLED);
					}
					else if (slre_compare_capture_multi(&captures[3], 2, "any", "*"))
					{
						out << "# installed=any" << std::endl ;
						pkgdata.setInstalledFilter(Engine::ANY);
					}
					else
					{
						out << "# Unknown value for installed" << std::endl ;
					}
				}
				else if (slre_compare_capture_multi(&captures[2], 4, "KEYWORD", "KEYW", "KW", "K"))
				{
					char *kw = strndup(captures[3].ptr, captures[3].len);
					pkgdata.setKeywordFilter(kw);
					out << "keyword=\"" << pkgdata.getKeywordFilter() << "\"" << std::endl ;
				}
				else if (slre_compare_capture_multi(&captures[2], 2, "TYPE", "T"))
				{
					out << "type=?" << std::endl ;
				}
				else if (slre_compare_capture_multi(&captures[2], 3, "INTERFACE", "IFACE", "IF"))
				{
					out << "interface=?" << std::endl ;
				}
				else
				{
					out << "# Unknown parameter name" << std::endl ;
				}

			}
			break;

			default:
			break;
		}
	}

	out << "# Stopping system" << std::endl;
	return true;
}


// Exported Stuff

extern "C" int init(int argc, const char *argv[])
{
	std::cout << _("CLI Plugin successfully loaded") << std::endl;
	return 1; // true
}

extern "C" void comment(const char *text)
{
	std::cout << "# " << text << std::endl;
}

extern "C" int go(PackageData &pkgdata)
{
	return Go(pkgdata);
}
