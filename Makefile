#!/usr/bin/make -f

# Copyright (C) 2007-2008  Miriam Ruiz <little_miry@yahoo.es>
#
# http://www.jdkoftinoff.com/main/Articles/Linux_Articles/ELF_Plugins/ 
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

MAJOR=0
MINOR=0

PKGCONFIG_FILES=libtagcoll2 boolstuff-0.1 libxdg-basedir
PKGCONFIG_CFLAGS= `pkg-config $(PKGCONFIG_FILES) --cflags`
DEFS=-DUSE_GETTEXT
STATIC_CFLAGS= -O2 -g -Wall $(DEFS)
CFLAGS= $(STATIC_CFLAGS) -fPIC
TEST_CFLAGS= -Wall -Werror $(CFLAGS)
LDFLAGS= -Wl,-z,defs -Wl,--as-needed -Wl,--no-undefined
PLUGINS_LDFLAGS= 
LIBS= -lept -lept-core -lapt-pkg -lxapian -ldl `pkg-config $(PKGCONFIG_FILES) --libs`

OBJS= Engine.o Environment.o filter.o field.o gofind.o \
	taghandler.o cfgmanager.o boolparser.o apthelper.o \
	dll.o guiplugin.o pkgdata.o slre.o i18n.o \
	utf8/parser.o utf8/html.o

LIB_OBJS= Engine.o Environment.o filter.o field.o \
	taghandler.o cfgmanager.o boolparser.o apthelper.o \
	dll.o guiplugin.o pkgdata.o slre.o i18n.o \
	utf8/parser.o utf8/html.o 

HEADERS=$(shell find . -name "*.h")

PLUGINS= gui_cli.so gui_fltk.so

all: libgofind.so libgofind.a gofind $(PLUGINS)

# the main executable has to be linked with the -rdynamic flag
# so the plug in libraries can call inherited methods and
# access vtables in the main executable.

gofind: libgofind.so libgofind.a gofind.o
	g++ -o $@ gofind.o -rdynamic $(LDFLAGS) -L. libgofind.a $(LIBS)

libgofind.so.$(MAJOR).$(MINOR): $(LIB_OBJS)
	g++ $(LDFLAGS) -shared \
		-Wl,-soname,libgofind.so.$(MAJOR) \
		-o libgofind.so.$(MAJOR).$(MINOR) \
		$+ -o $@ $(LIBS)

libgofind.so: libgofind.so.$(MAJOR).$(MINOR)
	rm -f $@.$(MAJOR)
	ln -s $@.$(MAJOR).$(MINOR) $@.$(MAJOR)
	rm -f $@
	ln -s $@.$(MAJOR) $@

libgofind.a: $(LIB_OBJS:.o=.static.o)
	ar cru $@ $+

gui_cli.so: gui_cli.o

gui_%.so : gui_%.o
	g++ $(PLUGINS_LDFLAGS) -shared $^ -o $@

gui_fltk.so : fltk/ui.h gui_fltk.o fltk/aux.o fltk/windows.o fltk/pkgbrowser.o fltk/ui.o
	g++ $(PLUGINS_LDFLAGS) -shared $^ -o $@ `fltk-config --ldflags --use-images` $(LIBS)

fltk/ui.h fltk/ui.cpp: fltk/ui.fld
	cd fltk && fluid -c -o ui.cpp -h ui.h ui.fld

gui_lua.so : gui_lua.o
	g++ $(PLUGINS_LDFLAGS) -shared $^ -o $@ `pkg-config lua5.1 --libs` $(LIBS)

gui_luagtk.so : gui_lua.o gui_luagtk.o
	g++ $(PLUGINS_LDFLAGS) -shared $^ -o $@ `pkg-config lua5.1 --libs` $(LIBS) -llua5.1-gtk

gui_lua.o: gui_lua.cpp
	g++ -o $@ -c $< $(CFLAGS) $(PKGCONFIG_CFLAGS) `pkg-config lua5.1 --cflags`

gui_luagtk.o: gui_luagtk.cpp
	g++ -o $@ -c $< $(CFLAGS) $(PKGCONFIG_CFLAGS) `pkg-config lua5.1 --cflags`

%.o: %.cpp $(HEADERS) Makefile
	g++ -o $@ -c $< $(CFLAGS) $(PKGCONFIG_CFLAGS)

%.o: %.c $(HEADERS) Makefile
	gcc -o $@ -c $< $(CFLAGS) $(PKGCONFIG_CFLAGS)

%.so : %.o
	g++ $(LDFLAGS) -shared $^ -o $@

%.static.o: %.cpp $(HEADERS) Makefile
	g++ -o $@ -c $< $(STATIC_CFLAGS) $(PKGCONFIG_CFLAGS)

%.static.o: %.c $(HEADERS) Makefile
	gcc -o $@ -c $< $(STATIC_CFLAGS) $(PKGCONFIG_CFLAGS)

TEST_OBJS= filter.test.o taghandler.test.o cfgmanager.test.o \
	boolparser.test.o slre.test.o apthelper.test.o \
	dll.test.o i18n.test.o utf8/testutf8.test.o \
	CuTest.o test.o utf8/parser.test.o utf8/html.test.o 
TEST_PLUGINS= testplugin.test.so

test: $(TEST_OBJS) $(TEST_PLUGINS)
	g++ -o $@ $+ -rdynamic -Wl,-rpath,. $(LDFLAGS) $(LIBS)

test.c:
	sh CuTest.sh > $@

test.o: test.c $(HEADERS) Makefile
	gcc -o $@ -DUNIT_TEST -c $< $(TEST_CFLAGS) $(PKGCONFIG_CFLAGS)

%.test.o: %.cpp $(HEADERS) Makefile
	g++ -o $@ -DUNIT_TEST -c $< $(TEST_CFLAGS) $(PKGCONFIG_CFLAGS)

%.test.o: %.c $(HEADERS) Makefile
	gcc -o $@ -DUNIT_TEST -c $< $(TEST_CFLAGS) $(PKGCONFIG_CFLAGS)

%.test.so : %.test.o
	g++ $(CFLAGS) $(PKGCONFIG_CFLAGS) -DUNIT_TEST -shared $^ -o $@

clean:
	rm -f gofind test test.c utf8/*.o cli/*.o fltk/*.o *.o *.so *.so* *.a
