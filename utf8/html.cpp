/*
 * Copyright (C) 2009  Miriam Ruiz <little_miry@yahoo.es>
 *
 * http://www.jdkoftinoff.com/main/Articles/Linux_Articles/ELF_Plugins/ 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "../common.h"
#include "parser.h"
#include "html.h"

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <iostream>
#include <string>
#include <map>

namespace utf8 { namespace html {

typedef struct {
	const char * Name;
	unsigned int Char;
	int UTF8Size;
	unsigned long int UTF8;
} HTMLEntityDataType;

#define UTF8SIZE(Ch)  ( (Ch >= 0x800) ? 3 : ( (Ch >= 0x80) ? 2 : 1 ) )
#define UTF8BYTE1(Ch) ( (Ch >= 0x800) ? (0xE0 | (Ch >> 12 & 0x0F)) : ( (Ch >= 0x80) ? (0xC0 | (Ch >> 6 & 0x1F)) : Ch ) )
#define UTF8BYTE2(Ch) ( (Ch >= 0x800) ? (0x80 | (Ch >> 6 & 0x3F)) : ( (Ch >= 0x80) ? (0x80 | (Ch & 0x3F)) : 0 ) )
#define UTF8BYTE3(Ch) ( (Ch >= 0x800) ? (0x80 | (Ch & 0x3F)) : ( (Ch >= 0x80) ? 0 : 0 ) )
#define UTF8ENTITY(Name, Ch) { Name, Ch, UTF8SIZE(Ch), UTF8BYTE1(Ch) + (UTF8BYTE2(Ch) << 8) + (UTF8BYTE3(Ch) << 16) }

static const HTMLEntityDataType HTMLEntityData[] =
{ // List of entities defined in the HTML 4.0 spec
	UTF8ENTITY("nbsp", 160),
	UTF8ENTITY("iexcl", 161),
	UTF8ENTITY("cent", 162),
	UTF8ENTITY("pound", 163),
	UTF8ENTITY("curren", 164),
	UTF8ENTITY("yen", 165),
	UTF8ENTITY("brvbar", 166),
	UTF8ENTITY("sect", 167),
	UTF8ENTITY("uml", 168),
	UTF8ENTITY("copy", 169),
	UTF8ENTITY("ordf", 170),
	UTF8ENTITY("laquo", 171),
	UTF8ENTITY("not", 172),
	UTF8ENTITY("shy", 173),
	UTF8ENTITY("reg", 174),
	UTF8ENTITY("macr", 175),
	UTF8ENTITY("deg", 176),
	UTF8ENTITY("plusmn", 177),
	UTF8ENTITY("sup2", 178),
	UTF8ENTITY("sup3", 179),
	UTF8ENTITY("acute", 180),
	UTF8ENTITY("micro", 181),
	UTF8ENTITY("para", 182),
	UTF8ENTITY("middot", 183),
	UTF8ENTITY("cedil", 184),
	UTF8ENTITY("sup1", 185),
	UTF8ENTITY("ordm", 186),
	UTF8ENTITY("raquo", 187),
	UTF8ENTITY("frac14", 188),
	UTF8ENTITY("frac12", 189),
	UTF8ENTITY("frac34", 190),
	UTF8ENTITY("iquest", 191),
	UTF8ENTITY("Agrave", 192),
	UTF8ENTITY("Aacute", 193),
	UTF8ENTITY("Acirc", 194),
	UTF8ENTITY("Atilde", 195),
	UTF8ENTITY("Auml", 196),
	UTF8ENTITY("Aring", 197),
	UTF8ENTITY("AElig", 198),
	UTF8ENTITY("Ccedil", 199),
	UTF8ENTITY("Egrave", 200),
	UTF8ENTITY("Eacute", 201),
	UTF8ENTITY("Ecirc", 202),
	UTF8ENTITY("Euml", 203),
	UTF8ENTITY("Igrave", 204),
	UTF8ENTITY("Iacute", 205),
	UTF8ENTITY("Icirc", 206),
	UTF8ENTITY("Iuml", 207),
	UTF8ENTITY("ETH", 208),
	UTF8ENTITY("Ntilde", 209),
	UTF8ENTITY("Ograve", 210),
	UTF8ENTITY("Oacute", 211),
	UTF8ENTITY("Ocirc", 212),
	UTF8ENTITY("Otilde", 213),
	UTF8ENTITY("Ouml", 214),
	UTF8ENTITY("times", 215),
	UTF8ENTITY("Oslash", 216),
	UTF8ENTITY("Ugrave", 217),
	UTF8ENTITY("Uacute", 218),
	UTF8ENTITY("Ucirc", 219),
	UTF8ENTITY("Uuml", 220),
	UTF8ENTITY("Yacute", 221),
	UTF8ENTITY("THORN", 222),
	UTF8ENTITY("szlig", 223),
	UTF8ENTITY("agrave", 224),
	UTF8ENTITY("aacute", 225),
	UTF8ENTITY("acirc", 226),
	UTF8ENTITY("atilde", 227),
	UTF8ENTITY("auml", 228),
	UTF8ENTITY("aring", 229),
	UTF8ENTITY("aelig", 230),
	UTF8ENTITY("ccedil", 231),
	UTF8ENTITY("egrave", 232),
	UTF8ENTITY("eacute", 233),
	UTF8ENTITY("ecirc", 234),
	UTF8ENTITY("euml", 235),
	UTF8ENTITY("igrave", 236),
	UTF8ENTITY("iacute", 237),
	UTF8ENTITY("icirc", 238),
	UTF8ENTITY("iuml", 239),
	UTF8ENTITY("eth", 240),
	UTF8ENTITY("ntilde", 241),
	UTF8ENTITY("ograve", 242),
	UTF8ENTITY("oacute", 243),
	UTF8ENTITY("ocirc", 244),
	UTF8ENTITY("otilde", 245),
	UTF8ENTITY("ouml", 246),
	UTF8ENTITY("divide", 247),
	UTF8ENTITY("oslash", 248),
	UTF8ENTITY("ugrave", 249),
	UTF8ENTITY("uacute", 250),
	UTF8ENTITY("ucirc", 251),
	UTF8ENTITY("uuml", 252),
	UTF8ENTITY("yacute", 253),
	UTF8ENTITY("thorn", 254),
	UTF8ENTITY("yuml", 255),
	UTF8ENTITY("fnof", 402),
	// Greek
	UTF8ENTITY("Alpha", 913),
	UTF8ENTITY("Beta", 914),
	UTF8ENTITY("Gamma", 915),
	UTF8ENTITY("Delta", 916),
	UTF8ENTITY("Epsilon", 917),
	UTF8ENTITY("Zeta", 918),
	UTF8ENTITY("Eta", 919),
	UTF8ENTITY("Theta", 920),
	UTF8ENTITY("Iota", 921),
	UTF8ENTITY("Kappa", 922),
	UTF8ENTITY("Lambda", 923),
	UTF8ENTITY("Mu", 924),
	UTF8ENTITY("Nu", 925),
	UTF8ENTITY("Xi", 926),
	UTF8ENTITY("Omicron", 927),
	UTF8ENTITY("Pi", 928),
	UTF8ENTITY("Rho", 929),
	UTF8ENTITY("Sigma", 931),
	UTF8ENTITY("Tau", 932),
	UTF8ENTITY("Upsilon", 933),
	UTF8ENTITY("Phi", 934),
	UTF8ENTITY("Chi", 935),
	UTF8ENTITY("Psi", 936),
	UTF8ENTITY("Omega", 937),
	UTF8ENTITY("alpha", 945),
	UTF8ENTITY("beta", 946),
	UTF8ENTITY("gamma", 947),
	UTF8ENTITY("delta", 948),
	UTF8ENTITY("epsilon", 949),
	UTF8ENTITY("zeta", 950),
	UTF8ENTITY("eta", 951),
	UTF8ENTITY("theta", 952),
	UTF8ENTITY("iota", 953),
	UTF8ENTITY("kappa", 954),
	UTF8ENTITY("lambda", 955),
	UTF8ENTITY("mu", 956),
	UTF8ENTITY("nu", 957),
	UTF8ENTITY("xi", 958),
	UTF8ENTITY("omicron", 959),
	UTF8ENTITY("pi", 960),
	UTF8ENTITY("rho", 961),
	UTF8ENTITY("sigmaf", 962),
	UTF8ENTITY("sigma", 963),
	UTF8ENTITY("tau", 964),
	UTF8ENTITY("upsilon", 965),
	UTF8ENTITY("phi", 966),
	UTF8ENTITY("chi", 967),
	UTF8ENTITY("psi", 968),
	UTF8ENTITY("omega", 969),
	UTF8ENTITY("thetasym", 977),
	UTF8ENTITY("upsih", 978),
	UTF8ENTITY("piv", 982),
	// General Punctuation
	UTF8ENTITY("bull", 8226),
	UTF8ENTITY("hellip", 8230),
	UTF8ENTITY("prime", 8242),
	UTF8ENTITY("Prime", 8243),
	UTF8ENTITY("oline", 8254),
	UTF8ENTITY("frasl", 8260),
	// Letterlike Symbols
	UTF8ENTITY("weierp", 8472),
	UTF8ENTITY("image", 8465),
	UTF8ENTITY("real", 8476),
	UTF8ENTITY("trade", 8482),
	UTF8ENTITY("alefsym", 8501),
	// Arrows
	UTF8ENTITY("larr", 8592),
	UTF8ENTITY("uarr", 8593),
	UTF8ENTITY("rarr", 8594),
	UTF8ENTITY("darr", 8595),
	UTF8ENTITY("harr", 8596),
	UTF8ENTITY("crarr", 8629),
	UTF8ENTITY("lArr", 8656),
	UTF8ENTITY("uArr", 8657),
	UTF8ENTITY("rArr", 8658),
	UTF8ENTITY("dArr", 8659),
	UTF8ENTITY("hArr", 8660),
	// Mathematical Operators
	UTF8ENTITY("forall", 8704),
	UTF8ENTITY("part", 8706),
	UTF8ENTITY("exist", 8707),
	UTF8ENTITY("empty", 8709),
	UTF8ENTITY("nabla", 8711),
	UTF8ENTITY("isin", 8712),
	UTF8ENTITY("notin", 8713),
	UTF8ENTITY("ni", 8715),
	UTF8ENTITY("prod", 8719),
	UTF8ENTITY("sum", 8721),
	UTF8ENTITY("minus", 8722),
	UTF8ENTITY("lowast", 8727),
	UTF8ENTITY("radic", 8730),
	UTF8ENTITY("prop", 8733),
	UTF8ENTITY("infin", 8734),
	UTF8ENTITY("and", 8743),
	UTF8ENTITY("or", 8744),
	UTF8ENTITY("cap", 8745),
	UTF8ENTITY("cup", 8746),
	UTF8ENTITY("int", 8747),
	UTF8ENTITY("there4", 8756),
	UTF8ENTITY("sim", 8764),
	UTF8ENTITY("cong", 8773),
	UTF8ENTITY("asymp", 8776),
	UTF8ENTITY("ne", 8800),
	UTF8ENTITY("equiv", 8801),
	UTF8ENTITY("le", 8804),
	UTF8ENTITY("ge", 8805),
	UTF8ENTITY("sub", 8834),
	UTF8ENTITY("sup", 8835),
	UTF8ENTITY("nsub", 8836),
	UTF8ENTITY("sube", 8838),
	UTF8ENTITY("supe", 8839),
	UTF8ENTITY("oplus", 8853),
	UTF8ENTITY("otimes", 8855),
	UTF8ENTITY("perp", 8869),
	UTF8ENTITY("sdot", 8901),
	// Miscellaneous Technical
	UTF8ENTITY("lceil", 8968),
	UTF8ENTITY("rceil", 8969),
	UTF8ENTITY("lfloor", 8970),
	UTF8ENTITY("rfloor", 8971),
	UTF8ENTITY("lang", 9001),
	UTF8ENTITY("rang", 9002),
	// Geometric Shapes
	UTF8ENTITY("loz", 9674),
	// Miscellaneous Symbols
	UTF8ENTITY("spades", 9824),
	UTF8ENTITY("clubs", 9827),
	UTF8ENTITY("hearts", 9829),
	UTF8ENTITY("diams", 9830),
	UTF8ENTITY("quot", 34),
	UTF8ENTITY("amp", 38),
	UTF8ENTITY("lt", 60),
	UTF8ENTITY("gt", 62),
	// Latin Extended-A
	UTF8ENTITY("OElig", 338),
	UTF8ENTITY("oelig", 339),
	UTF8ENTITY("Scaron", 352),
	UTF8ENTITY("scaron", 353),
	UTF8ENTITY("Yuml", 376),
	// Spacing Modifier Letters
	UTF8ENTITY("circ", 710),
	UTF8ENTITY("tilde", 732),
	// General Punctuation
	UTF8ENTITY("ensp", 8194),
	UTF8ENTITY("emsp", 8195),
	UTF8ENTITY("thinsp", 8201),
	UTF8ENTITY("zwnj", 8204),
	UTF8ENTITY("zwj", 8205),
	UTF8ENTITY("lrm", 8206),
	UTF8ENTITY("rlm", 8207),
	UTF8ENTITY("ndash", 8211),
	UTF8ENTITY("mdash", 8212),
	UTF8ENTITY("lsquo", 8216),
	UTF8ENTITY("rsquo", 8217),
	UTF8ENTITY("sbquo", 8218),
	UTF8ENTITY("ldquo", 8220),
	UTF8ENTITY("rdquo", 8221),
	UTF8ENTITY("bdquo", 8222),
	UTF8ENTITY("dagger", 8224),
	UTF8ENTITY("Dagger", 8225),
	UTF8ENTITY("permil", 8240),
	UTF8ENTITY("lsaquo", 8249),
	UTF8ENTITY("rsaquo", 8250),
	UTF8ENTITY("euro", 8364),
	UTF8ENTITY(NULL, 0) // End of the list
}; // End of HTMLEntityData

class Char2UTF8Map : std::map<unsigned long int, std::string>
{
public:
	typedef std::map<unsigned long int, std::string>::iterator iterator;
	typedef std::pair<unsigned long int, std::string> pair;
	Char2UTF8Map(const HTMLEntityDataType *entities)
	{
		const HTMLEntityDataType *ent = HTMLEntityData;
		while (ent->Name)
		{
			insert ( pair(ent->Char,ent->Name) );
			ent++;
		}
	}
	inline iterator end()
	{
		return std::map<unsigned long int, std::string>::end();
	}
	inline iterator find(unsigned long int ch)
	{
		return std::map<unsigned long int, std::string>::find(ch);
	}
};

static Char2UTF8Map char2utf8(HTMLEntityData);

void QuoteHTML(std::ostream &out, const char *in)
{
	const char *ptr = in;
	int len = strlen(in);
	while (*ptr)
	{
		unsigned long int ch;
		int adv = utf8::parser::utf8_to_char(&ch, ptr, len);
		if (adv && len >= adv)
		{
			Char2UTF8Map::iterator it = char2utf8.find(ch);
			if (ch < 128)
			{
				out.put(ch);
			}
			else if (it != char2utf8.end())
			{
				out << "&" << it->second << ";";
			}
			else
			{
				out << "&#" << ch << ";";
			}
			len -= adv;
			ptr += adv;
		}
		else
			break;
	}
}

void UTF8ToLatin1(std::ostream &out, const char *in)
{
	const char *ptr = in;
	int len = strlen(in);
	while (*ptr)
	{
		unsigned long int ch;
		int adv = utf8::parser::utf8_to_char(&ch, ptr, len);
		if (adv && len >= adv)
		{
			Char2UTF8Map::iterator it = char2utf8.find(ch);
			if (ch < 256)
				out.put(ch);
			else
				out << "?";
			len -= adv;
			ptr += adv;
		}
		else
			break;
	}
}

} } // Close namespaces

#ifdef UNIT_TEST

using namespace utf8::html;

static uint32_t Ch2UTF8(unsigned int ch) // Writes ch in UTF-8 encoding. Only 16 bits
{
	if (ch >= 0x800)
	{
		return (0xE0 | (ch >> 12 & 0x0F)) + (0x80 | (ch >> 6 & 0x3F)) * 256 + (0x80 | (ch & 0x3F)) * 65536;
	}
	else if (ch >= 0x80)
	{
		return (0xC0 | (ch >> 6 & 0x1F)) + (0x80 | (ch & 0x3F)) * 256;
	}
	else
		return ch;
}

TEST_FUNCTION TestCuUTF8HTML(CuTest* tc)
{
	const HTMLEntityDataType *ent = HTMLEntityData;
	while (ent->Name)
	{
		uint32_t utf8 = Ch2UTF8(ent->Char);
		CuAssertTrue(tc, memcmp(&ent->UTF8, &utf8, 3) == 0 );
		ent++;
	}
	QuoteHTML(std::cout, "Hou>stño€n\n");
	std::cout << std::endl;

}
#endif

