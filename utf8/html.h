/*
 * Copyright (C) 2009  Miriam Ruiz <little_miry@yahoo.es>
 *
 * http://www.jdkoftinoff.com/main/Articles/Linux_Articles/ELF_Plugins/ 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _GOPLAY_UTF8_PARSER_H
#define _GOPLAY_UTF8_PARSER_H

#include <cstdlib>

namespace utf8
{
	namespace html
	{

		void QuoteHTML(std::ostream &out, const char *in);
		void UTF8ToLatin1(std::ostream &out, const char *in);

	} // namespace utf8::html
} // namespace utf8

#endif // _GOPLAY_UTF8_PARSER_H
