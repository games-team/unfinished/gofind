/* 
 * helper_utf8.c - Raptor UTF-8 and Unicode support
 *
 * Copyright (C) 2002-2006, David Beckett http://purl.org/net/dajobe/
 * Copyright (C) 2002-2004, University of Bristol, UK http://www.bristol.ac.uk/
 * Copyright (C) 2009  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This package is Free Software and part of Redland http://librdf.org/
 *
 * It is licensed under the following three licenses as alternatives:
 *   1. GNU Lesser General Public License (LGPL) V2.1 or any newer version
 *   2. GNU General Public License (GPL) V2 or any newer version
 *   3. Apache License, V2.0 or any newer version
 *
 * You may not use this file except in compliance with at least one of
 * the above three licenses.
 */

#ifndef _GOPLAY_UTF8_PARSER_H
#define _GOPLAY_UTF8_PARSER_H

#include <cstdlib>

namespace utf8
{
	namespace parser
	{

		int char_to_utf8(unsigned long c, char *output);
		int utf8_to_char(unsigned long *output, const char *input, int length);

		int is_letter(long c);
		int is_basechar(long c);
		int is_ideographic(long c);
		int is_combiningchar(long c);
		int is_digit(long c);
		int is_extender(long c);

		int is_space(long c);

		int is_xml11_namestartchar(long c);
		int is_xml10_namestartchar(long c);
		int is_xml11_namechar(long c);
		int is_xml10_namechar(long c);

		int is_nfc(const char *input, size_t length);
		int check(const char *string, size_t length);

	} // namespace utf8::parser
} // namespace utf8

#endif // _GOPLAY_UTF8_PARSER_H
