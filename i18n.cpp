/*
 * Copyright (C) 2005  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "i18n.h"

#ifdef USE_GETTEXT

#include <libintl.h>
#include <locale.h>

GettextInit::GettextInit()
{
	if  (!IsInit)
	{
		IsInit = true;
		setlocale (LC_MESSAGES, "");
		setlocale (LC_CTYPE, "");
		setlocale (LC_COLLATE, "");
		textdomain ("gofind");
		bindtextdomain ("gofind", NULL);
	}
}

bool GettextInit::IsInit = false;

#endif // USE_GETTEXT
