/*
 * Copyright (C) 2005  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _games_i18n_h
#define _games_i18n_h

#ifdef USE_GETTEXT
#include <libintl.h>
#include <locale.h>
#else
// Work-around until goplay is actually gettextised
#define gettext(a) (a)
#endif

#define _(String) gettext (String)

#ifdef USE_GETTEXT
class GettextInit
{
	protected:
		static bool IsInit;
	public:
		GettextInit();
};

static GettextInit gettext_init;
#endif

#endif // _games_i18n_h
