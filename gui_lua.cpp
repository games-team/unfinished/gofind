/*
 * Copyright (C) 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "common.h"
#include "filter.h"
#include "slre.h"
#include "pkgdata.h"
#include "lua/luna.h"

#include <typeinfo>
#include <iostream>
#include <string>

#include <ept/apt/packagerecord.h>
#include <ept/textsearch/textsearch.h>
#include <wibble/regexp.h>
#include <wibble/string.h>
#include <xapian.h>

#include <iostream>
#include <cmath>

#include <stdlib.h>
#include <stdio.h>

#ifdef USE_GETTEXT
#include <libintl.h>
#include <locale.h>
#define _(String) gettext (String)
#else
// Work-around until goplay is actually gettextised
#define gettext(String) (String)
#define _(String) (String)
#endif

using namespace std;
using namespace ept;
using namespace ept::debtags;
using namespace ept::apt;
using namespace ept::textsearch;

/******************************************************************************
 * Copyright (C) 1994-2008 Lua.org, PUC-Rio.  All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define lua_c

#include <lua.hpp>

static lua_State *globalL = NULL;

static void lstop (lua_State *L, lua_Debug *ar)
{
	(void)ar; /* unused arg. */
	lua_sethook(L, NULL, 0, 0);
	luaL_error(L, "interrupted!");
}


static void laction (int i)
{
	signal(i, SIG_DFL);
	/* if another SIGINT happens before lstop, terminate process (default action) */
	lua_sethook(globalL, lstop, LUA_MASKCALL | LUA_MASKRET | LUA_MASKCOUNT, 1);
}


static void l_message (const char *msg)
{
	fprintf(stderr, "%s\n", msg);
	fflush(stderr);
}


static int report (lua_State *L, int status)
{
	if (status && !lua_isnil(L, -1))
	{
		const char *msg = lua_tostring(L, -1);
		if (msg == NULL) msg = "(error object is not a string)";
		l_message(msg);
		lua_pop(L, 1);
	}
	return status;
}


static int traceback (lua_State *L)
{
	if (!lua_isstring(L, 1)) /* 'message' not a string? */
		return 1; /* keep it intact */
	lua_getfield(L, LUA_GLOBALSINDEX, "debug");
	if (!lua_istable(L, -1))
	{
		lua_pop(L, 1);
		return 1;
	}
	lua_getfield(L, -1, "traceback");
	if (!lua_isfunction(L, -1))
	{
		lua_pop(L, 2);
		return 1;
	}
	lua_pushvalue(L, 1); /* pass error message */
	lua_pushinteger(L, 2); /* skip this function and traceback */
	lua_call(L, 2, 1); /* call debug.traceback */
	return 1;
}


static int docall (lua_State *L, int narg, int clear)
{
	int status;
	/* function index */
	int base = lua_gettop(L) - narg;
	/* push traceback function */
	lua_pushcfunction(L, traceback);
	lua_insert(L, base); /* put it under chunk and args */
	signal(SIGINT, laction);
	status = lua_pcall(L, narg, (clear ? 0 : LUA_MULTRET), base);
	signal(SIGINT, SIG_DFL);
	lua_remove(L, base); /* remove traceback function */
	/* force a complete garbage collection in case of errors */
	if (status != 0) lua_gc(L, LUA_GCCOLLECT, 0);
	return status;
}


static void print_version (void)
{
	l_message(LUA_RELEASE "  " LUA_COPYRIGHT);
}


static int getargs (lua_State *L, const char **argv)
{
	int n = 0;
	int argc = 0;

	while (argv[argc]) argc++;  /* count total number of arguments */
	int narg = argc - (n + 1);  /* number of arguments to the script */
	luaL_checkstack(L, narg + 3, "too many arguments to script");

	int i;
	for (i=n+1; i < argc; i++)
		lua_pushstring(L, argv[i]);
	lua_createtable(L, narg, n + 1);
	for (i=0; i < argc; i++)
	{
		lua_pushstring(L, argv[i]);
		lua_rawseti(L, -2, i - n);
	}
	return narg;
}


static int dofile (lua_State *L, const char *name)
{
	int status = luaL_loadfile(L, name) || docall(L, 0, 1);
	return report(L, status);
}


static int dostring (lua_State *L, const char *s, const char *name)
{
	int status = luaL_loadbuffer(L, s, strlen(s), name) || docall(L, 0, 1);
	return report(L, status);
}


static int dolibrary (lua_State *L, const char *name)
{
	lua_getglobal(L, "require");
	lua_pushstring(L, name);
	return report(L, docall(L, 1, 1));
}


static int handle_script (lua_State *L, const char *fname, const char **argv)
{
	int status;
	/* collect arguments */
	int narg = getargs(L, argv);
	lua_setglobal(L, "arg");
	status = luaL_loadfile(L, fname);
	lua_insert(L, -(narg+1));
	if (status == 0)
		status = docall(L, narg, 0);
	else
		lua_pop(L, narg);
	return report(L, status);
}


static int load_library (lua_State *L, const char *filename)
{
	lua_assert(filename != NULL);
	if (dolibrary(L, filename))
		return 1; /* stop if file fails */
	return 0;
}


static int handle_luainit (lua_State *L)
{
	const char *init = getenv(LUA_INIT);
	if (init == NULL) return 0; /* status OK */
	else if (init[0] == '@')
		return dofile(L, init+1);
	else
		return dostring(L, init, "=" LUA_INIT);
}


struct Smain
{
	Smain(int _argc, const char **_argv) : argc(_argc), argv(_argv), status(0) { }
	const int argc;
	const char **argv;
	int status;
};

static int pmain (lua_State *L)
{
	struct Smain *s = (struct Smain *)lua_touserdata(L, 1);
	globalL = L;

	lua_gc(L, LUA_GCSTOP, 0); /* stop collector during initialization */
	luaL_openlibs(L); /* open libraries */
	lua_gc(L, LUA_GCRESTART, 0);

	s->status = handle_luainit(L);
	if (s->status != 0) return 0;

	printf("Starting LUA Script\n");
	if (s->argv[0])
		s->status = handle_script(L, s->argv[0], s->argv);
	printf("Finished LUA Script\n");

	if (s->status != 0) return 0;
	return 0;
}


static int argc = 0;
static const char **argv = NULL;

extern "C" int go(PackageData &pkgdata)
{
	int status;

	lua_State *L = lua_open(); /* create state */
	if (L == NULL)
	{
		l_message("cannot create state: not enough memory");
		return EXIT_FAILURE;
	}

	struct Smain s(argc, argv);
	status = lua_cpcall(L, &pmain, &s);
	report(L, status);
	lua_close(L);
	return (status || s.status) ? 0 /* FAILURE */ : 1 /* SUCCESS */;
}

extern "C" int init(int _argc, const char *_argv[])
{
	std::cout << _("Lua Plugin successfully loaded") << std::endl;
	argv = new const char *[_argc+2];
	argc = 1;
	argv[0] = "lua/script.lua";
	for (int i=0; i<_argc && (argv[i+1]=_argv[i]) ; i++, argc++)
	{
	}
	argv[argc] = NULL;
	return 1; // true
}

extern "C" void end()
{
	if (argv) delete[] argv;
	std::cout << _("Lua Plugin stopped") << std::endl;
}

extern "C" void comment(const char *text)
{
	std::cout << "# " << text << std::endl;
}
