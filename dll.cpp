/*
 * Copyright (C) 1995  Jeff Koftinoff <jeffk@jdkoftinoff.com>
 * Copyright (C) 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * http://www.jdkoftinoff.com/main/Articles/Linux_Articles/ELF_Plugins/ 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "dll.h"

#include <typeinfo>
#include <iostream>
#include <string>
#include <dlfcn.h>

DLLManager::DLLManager( const char *fname )
{
	// Try to open the library now and get any error message.

	dlerror(); // Clean previous errors, if any
	h=dlopen( fname, RTLD_NOW );
	err=dlerror();
}

DLLManager::~DLLManager()
{
	// close the library if it isn't null
	if( h!=0 )
	dlclose(h);
}

bool DLLManager::GetSymbol(
	void **v,
	const char *sym_name
	)
{
	// try extract a symbol from the library
	// get any error message is there is any

	dlerror(); // Clear previous error, just in case

	if( h!=0 )
	{
		*v = dlsym( h, sym_name );
		err=dlerror();
		if( err==0 )
			return true;
		else
			return false;
	}
	else
	{
		return false;
	}
}

DLLFactoryBase::DLLFactoryBase(
	const char *fname,
	const char *factory
	) : DLLManager(fname)
{
	// try get the factory function if there is no error yet

	factory_func=0;

	if( LastError()==0 )
	{
		GetSymbol( (void **)&factory_func, factory ? factory : "factory0" );
	}
}

DLLFactoryBase::~DLLFactoryBase()
{
}

#ifdef UNIT_TEST

#include "testplugin.h"
#include "CuTest.h"

TEST_FUNCTION TestCuTestPlugIn(CuTest* tc)
{
	std::cout << "Loading Test Plugin: \"testplugin.test.so\"" << std::endl;
	DLLFactory<TestPlugInFactory> factory( "testplugin.test.so" );
	CuAssertTrue(tc, factory.LastError() == NULL );
	if (factory.LastError() != NULL) return;
	TestPlugIn *plugin=factory.factory->CreatePlugIn();
	CuAssertTrue(tc, plugin != NULL );
	if (!plugin) return;
	CuAssertTrue(tc, plugin->GetTrue() == true );
	CuAssertTrue(tc, plugin->GetFalse() == false );
	CuAssertTrue(tc, plugin->GetSame(true) == true );
	CuAssertTrue(tc, plugin->GetSame(false) != true );
	CuAssertTrue(tc, plugin->GetOther(true) == false );
	CuAssertTrue(tc, plugin->GetOther(false) != false );
	delete plugin;
}

#endif
