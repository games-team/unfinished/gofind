/*
 * Copyright (C) 1995  Jeff Koftinoff <jeffk@jdkoftinoff.com>
 * Copyright (C) 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * http://www.jdkoftinoff.com/main/Articles/Linux_Articles/ELF_Plugins/ 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _GOFIND_GUIPLUGIN_H
#define _GOFIND_GUIPLUGIN_H

#include "dll.h"
#include "pkgdata.h"

#include <iostream>

using namespace ept;

class GUIPlugIn : public DLLManager
{
 public:
	GUIPlugIn(const char *filename);
	virtual ~GUIPlugIn();

	void Comment(const char *szFormat, ...);

	inline int Init(int argc, const char *argv[])
	{
		if (init) return init(argc, argv);
		std::cout << _("No init function found in plugin.") << std::endl;
		return -1; // error
	}

	inline int Go(PackageData &pkgdata)
	{
		if (go) return go(pkgdata);
		std::cout << _("No go function found in plugin.") << std::endl;
		return -1; // error
	}

 protected:
	int (*init) (int argc, const char *argv[]);
	void (*comment) (const char *text);
	int (*go) (PackageData &pkgdata);
};

#endif // _GOFIND_GUIPLUGIN_H
