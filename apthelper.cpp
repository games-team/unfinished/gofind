/*
 * Copyright (C) 2009  Miriam Ruiz <little_miry@yahoo.es>
 *
 * Based on cmdline/apt-cache.cc from apt-0.7.21
 *
 * Apt is copyright 1997, 1998, 1999 Jason Gunthorpe and others.
 * Apt is currently developed by APT Development Team <deity@lists.debian.org>.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "apthelper.h"
#include "common.h"
#include "i18n.h"

#include <apt-pkg/error.h>
#include <apt-pkg/pkgcachegen.h>
#include <apt-pkg/init.h>
#include <apt-pkg/progress.h>
#include <apt-pkg/sourcelist.h>
#include <apt-pkg/cmndline.h>
#include <apt-pkg/strutl.h>
#include <apt-pkg/pkgrecords.h>
#include <apt-pkg/srcrecords.h>
#include <apt-pkg/version.h>
#include <apt-pkg/policy.h>
#include <apt-pkg/tagfile.h>
#include <apt-pkg/algorithms.h>
#include <apt-pkg/sptr.h>

#include <locale.h>
#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <regex.h>
#include <cstdio>

#include <iomanip>

#ifdef UNIT_TEST
#include "CuTest.h"
#endif

using namespace std;

bool AptHelper::Init = false;

AptHelper::AptHelper(int argc,const char *argv[]) : GCache(NULL), SrcList(NULL), Map(NULL)
{
	if (Init) return;

	CommandLine::Args Args[] =
	{
		{0, "pkg-cache",        "Dir::Cache::pkgcache",       CommandLine::HasArg},
		{0, "src-cache",        "Dir::Cache::srcpkgcache",    CommandLine::HasArg},
		{0, "important",        "APT::Cache::Important",      0},
		{0, "full",             "APT::Cache::ShowFull",       0},
		{0, "generate",         "APT::Cache::Generate",       0},
		{0, "all-versions",     "APT::Cache::AllVersions",    0},
		{0, "names-only",       "APT::Cache::NamesOnly",      0},
		{0, "all-names",        "APT::Cache::AllNames",       0},
		{0, "recurse",          "APT::Cache::RecurseDepends", 0},
		{0, "apt-config-file",   0,                           CommandLine::ConfigFile},
		{0, "apt-option",        0,                           CommandLine::ArbItem},
		{0, "installed",         "APT::Cache::Installed",     0},
		{0,0,0,0}
	};

	_config->Set("quiet",0);
	_config->Set("help",false);

	// Parse the command line and initialize the package library
	CommandLine CmdL(Args, _config);
	if (pkgInitConfig(*_config) == false ||
		CmdL.Parse(argc,argv) == false ||
		pkgInitSystem(*_config,_system) == false)
	{
		_error->DumpErrors();
		Init = false;
		return;
	}

	// Deal with stdout not being a tty
	if (isatty(STDOUT_FILENO) && _config->FindI("quiet",0) < 1)
		_config->Set("quiet","1");

	if (_error->PendingError() == false)
	{
		if (_config->FindB("APT::Cache::Generate",true) == false)
		{
			Map = new MMap(*new FileFd(_config->FindFile("Dir::Cache::pkgcache"),
				FileFd::ReadOnly),MMap::Public|MMap::ReadOnly);
		}
		else
		{
			// Open the cache file
			SrcList = new pkgSourceList;
			SrcList->ReadMainList();

			// Generate it and map it
			OpProgress Prog;
			pkgMakeStatusCache(*SrcList,Prog,&Map,true);
		}

		if (_error->PendingError() == false)
		{
			GCache = new pkgCache(Map);
		}
	}

	Init = true;
}

AptHelper::~AptHelper()
{
	if (!Init) return;

	if (Map)
	{
		delete Map;
		Map = NULL;
	}

	// Print any errors or warnings found during parsing
	if (_error->empty() == false)
	{
		//bool Errors = _error->PendingError();
		_error->DumpErrors();
	}

	Init = false;
}

bool AptHelper::GetPackageInfo(const std::string &package_name, AptHelper::PackageInfo *info)
{
	info->Clean();

	if (!IsInit() || !GCache || _error->PendingError() != false)
		return false;

	pkgCache &Cache = *GCache;
	pkgDepCache::Policy Plcy;

	unsigned found = 0;

	pkgCache::PkgIterator Pkg = Cache.FindPkg(package_name);
	if (Pkg.end() == true)
	{
		_error->Warning(_("Unable to locate package %s"), package_name.c_str());
		return _error->Error(_("No packages found"));;
	}

	++found;

	// Find the proper version to use.
	if (_config->FindB("APT::Cache::AllVersions","true") == true)
	{
		pkgCache::VerIterator V;
		for (V = Pkg.VersionList(); V.end() == false; V++)
		{
			// Show the right description
			pkgRecords Recs(*GCache);
			pkgCache::DescIterator Desc = V.TranslatedDescription();
			pkgRecords::Parser &P = Recs.Lookup(Desc.FileList());
			info->ShortDescription = P.ShortDesc();
			info->LongDescription = P.LongDesc().substr(P.LongDesc().find("\n")+1);
			info->LanguageCode = Desc.LanguageCode();
			info->Name = package_name;
			break;
		}
	}
	else
	{
		pkgCache::VerIterator V = Plcy.GetCandidateVer(Pkg);
		if (V.end() == true || V.FileList().end() == true)
			return _error->Error(_("No packages found"));

		// Show the right description
		pkgRecords Recs(*GCache);
		pkgCache::DescIterator Desc = V.TranslatedDescription();
		pkgRecords::Parser &P = Recs.Lookup(Desc.FileList());
		info->ShortDescription = P.ShortDesc();
		info->LongDescription = P.LongDesc().substr(P.LongDesc().find("\n")+1);
		info->LanguageCode = Desc.LanguageCode();
		info->Name = package_name;
	}

	if (found > 0)
		return true;
	return _error->Error(_("No packages found"));
}

bool AptHelper::ShowPackage(const std::string &package_name)
{
	PackageInfo info;
	if (!GetPackageInfo(package_name, &info))
		return false;
	std::cout << "Description"
		<< ( (strcmp(info.LanguageCode.c_str(),"") != 0) ? "-" : "" ) << info.LanguageCode
		<< ": " << info.ShortDescription << std::endl << info.LongDescription << std::endl;
	return true;
}

int AptHelper::LocalityCompare(const void *a, const void *b)
{
	pkgCache::VerFile *A = *(pkgCache::VerFile **)a;
	pkgCache::VerFile *B = *(pkgCache::VerFile **)b;

	if (A == 0 && B == 0)
		return 0;
	if (A == 0)
		return 1;
	if (B == 0)
		return -1;

	if (A->File == B->File)
		return A->Offset - B->Offset;
	return A->File - B->File;
}

#ifdef UNIT_TEST
TEST_FUNCTION TestCuAptHelper(CuTest* tc)
{
	std::cerr << "TestCuAptHelper(CuTest* tc)" << std::endl;

	const char *args[2];
	args[0] = "./test";
	args[1] = NULL;

	AptHelper helper(1, args);
	CuAssertTrue(tc, helper.IsInit() == true );
	CuAssertTrue(tc, helper.ShowPackage("3dchess") == true );
}
#endif
