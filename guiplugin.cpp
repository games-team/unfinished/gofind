/*
 * Copyright (C) 1995  Jeff Koftinoff <jeffk@jdkoftinoff.com>
 * Copyright (C) 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * http://www.jdkoftinoff.com/main/Articles/Linux_Articles/ELF_Plugins/ 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "i18n.h"

#include "common.h"
#include "guiplugin.h"

#include <typeinfo>
#include <iostream>

#include <stdarg.h>
#include <string.h>

GUIPlugIn::GUIPlugIn(const char *filename) :
	DLLManager(filename),
	init(NULL),
	comment(NULL),
	go(NULL)
{
	if( LastError() )
		std::cout << _("Error loading GUI plugin: ") << LastError() << std::endl;

	GetSymbol((void **)&init, "init" );
	if( LastError() )
		std::cout << _("Error loading GUI plugin: ") << LastError() << std::endl;

	GetSymbol((void **)&comment, "comment" );
	if( LastError() )
		std::cout << _("Error loading GUI plugin: ") << LastError() << std::endl;

	GetSymbol((void **)&go, "go" );
	if( LastError() )
		std::cout << _("Error loading GUI plugin: ") << LastError() << std::endl;
}

GUIPlugIn::~GUIPlugIn()
{
}

void GUIPlugIn::Comment(const char *szFormat, ...)
{
	if (comment)
	{
		char str[4096];
		va_list arglist;

		*str = '\0';

		va_start(arglist, szFormat);
		vsprintf(str + strlen(str), szFormat, arglist);
		va_end(arglist);

		comment(str);
	}
	else std::cout << _("No comment function found in plugin.") << std::endl;
}
