/*
 * Copyright (C) 2007  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "windows.h"
#include "ui.h"

#include <stdio.h>

MainWindow::MainWindow(int w, int h, const char *label) : Fl_Double_Window(w, h, label)
{
}

MainWindow::MainWindow(int x, int y, int w, int h, const char *label) : Fl_Double_Window(x, y, w, h, label)
{
}

void MainWindow::resize(int x, int y, int w, int h)
{
	Fl_Double_Window::resize(x, y, w, h);
	Fl_Group *highest_parent=this;
	while (highest_parent->parent()) highest_parent = highest_parent->parent();
	GamesUI *ui = highest_parent ? (GamesUI*)(highest_parent->user_data()) : NULL;
	ui->Resize(Fl_Double_Window::w(), Fl_Double_Window::h());
}
