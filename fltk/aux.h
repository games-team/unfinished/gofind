/*
 * Copyright (C) 2007  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _GOFIND_FLTK_AUX_H
#define _GOFIND_FLTK_AUX_H

#include <FL/Fl.H>
#include <FL/Fl_Help_View.H>
#include <string>

class HTMLView : public Fl_Help_View
{
public:
	HTMLView(int xx, int yy, int ww, int hh, const char *l = 0);
	~HTMLView();

	static bool ExternalBrowser(const std::string &uri);
	static bool ExternalBrowserScreenshot(const std::string &pkg);
	static bool ExternalBrowserPackage(const std::string &pkg);
	static bool ExternalBrowserQAPackage(const std::string &pkg);

	typedef void DrawCallback(HTMLView *html_view);
	inline void SetDrawCallback(DrawCallback *function) { draw_cb = function;}

private:
	static const char *link_cb(Fl_Widget *w, const char *uri);

	DrawCallback *draw_cb;
};

#endif // _GOFIND_FLTK_AUX_H
