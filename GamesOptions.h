#ifndef _GOFIND_GAMESOPTIONS_H
#define _GOFIND_GAMESOPTIONS_H

/*
 * Commandline parser for the game viewer
 *
 * Copyright (C) 2003,2004,2005,2006,2007  Enrico Zini
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <wibble/commandline/parser.h>

namespace wibble {
namespace commandline {

struct GamesOptions : public StandardParserWithManpage
{
public:
	BoolOption* out_debug;
	BoolOption* out_verbose;
	StringOption* gui;
	StringOption* filter;
	StringOption* gowhere;
	StringOption* mainFacet;
	StringOption* secondaryFacet;
	StringOption* ftags;

	GamesOptions() 
		: StandardParserWithManpage("goplay", VERSION, 1, "Enrico Zini <enrico@enricozini.org> and Miriam Ruiz <miriam@debian.org>")
	{
		usage = "[options and arguments]";
		description = "Debian game browser";
		longDescription =
			"GoFind! is a Graphical User Interface (GUI) that uses DebTags"
			" for finding programs in Debian easily.\n";

		// add( name, shortName, longName, usage, description )

		out_verbose = add<BoolOption>("verbose", 'v', "verbose", "",
						"enable verbose output");
		out_debug = add<BoolOption>("debug", 0, "debug", "",
						"enable debugging output (including verbose output)");
		gui = add<StringOption>("gui", 0, "gui", "plugin",
						"select the user interface flavour. ");
		filter = add<StringOption>("filter", 0, "filter", "file",
						"select the filter configuration file. ");
		gowhere = add<StringOption>("go", 0, "go", "where",
						"change the interface flavour. "
						"Available flavours are: play, learn, admin, net, office, safe, web");
		mainFacet = add<StringOption>("primary", 0, "primary", "facet",
						"use the given facet instead of 'games'");
		secondaryFacet = add<StringOption>("secondary", 0, "secondary", "facet",
						"use the given facet instead of 'interface'");
		ftags = add<StringOption>("ftags", 0, "ftags", "tags",
						"comma-separated list of tags that are always "
						"required in search results (default: role::program)");
	}
};

}
}

// vim:set ts=4 sw=4:

#endif // _GOFIND_GAMESOPTIONS_H
