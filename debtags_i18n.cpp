#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <strings.h>

using namespace std;

#define DEBTAGS_VOCABULARY "/var/lib/debtags/vocabulary"

class DebtagsTag
{
public:
	std::string Name;
	std::string ShortDescription;
	std::string LongDescription;
};

class DebtagsFacet : public std::map<std::string, DebtagsTag>
{
public:
	std::string Name;
	std::string ShortDescription;
	std::string LongDescription;

	typedef std::map<std::string, DebtagsTag>::iterator iterator;
	typedef std::pair<std::string, DebtagsTag> pair;
	DebtagsFacet()
		{ }
	inline iterator end()
		{ return std::map<std::string, DebtagsTag>::end(); }
	inline iterator find(std::string id)
		{ return std::map<std::string, DebtagsTag>::find(id); }
	bool Write(std::ostream &out);
	bool WritePot(std::ostream &out);
};

bool DebtagsFacet::Write(std::ostream &out)
{
	iterator p;
	for(p = begin(); p != end(); p++)
	{
		out << "Tag: " << Name << "::" << p->first << std::endl;
		out << "Description: " << p->second.ShortDescription << std::endl;
		out << p->second.LongDescription << std::endl;
	}
	return true;
}

bool DebtagsFacet::WritePot(std::ostream &out)
{
	iterator p;
	for(p = begin(); p != end(); p++)
	{
		out << "# Tag: " << Name << "::" << p->first << std::endl;
		out << "# In facet: " << Name << " (" << ShortDescription << ") " << std::endl;
		out << "msgid \"" << p->second.ShortDescription << "\"" << std::endl;
		out << "msgstr \"\"" << std::endl;
//		out << p->second.LongDescription << std::endl;
		out << std::endl;
	}
	return true;
}

class DebtagsMap : public std::map<std::string, DebtagsFacet>
{
public:
	typedef std::map<std::string, DebtagsFacet>::iterator iterator;
	typedef std::pair<std::string, DebtagsFacet> pair;
	DebtagsMap()
	{
//		while (ent->Name)
//		{
//			insert ( pair(ent->Char,ent->Name) );
//			ent++;
//		}
	}
	inline iterator end()
	{
		return std::map<std::string, DebtagsFacet>::end();
	}
	inline iterator find(std::string id)
	{
		return std::map<std::string, DebtagsFacet>::find(id);
	}

	DebtagsFacet &AddFacet(const std::string &facet, const std::string &shortdesc, const std::string &longdesc);
	DebtagsTag &AddTag(const std::string &facet, const std::string &tag, const std::string &shortdesc, const std::string &longdesc);
	bool Read(std::istream &in);
	bool Write(std::ostream &out);
	bool WritePot(std::ostream &out);
};

DebtagsFacet &DebtagsMap::AddFacet(const std::string &facet, const std::string &shortdesc, const std::string &longdesc)
{
	DebtagsFacet &f = operator[] (facet);
	f.Name = facet;
	f.ShortDescription = shortdesc;
	f.LongDescription = longdesc;
	return f;
}

DebtagsTag &DebtagsMap::AddTag(const std::string &facet, const std::string &tag, const std::string &shortdesc, const std::string &longdesc)
{
	DebtagsFacet &f = operator[] (facet);
	DebtagsTag &t = f.operator[] (tag);
	t.Name = tag;
	t.ShortDescription = shortdesc;
	t.LongDescription = longdesc;
	return t;
}

bool DebtagsMap::Read(std::istream &in)
{
	enum { ItemNone = 0, ItemFacet = 1, ItemTag = 2 }; // Powers of two
	enum { StatusNone = 0, StatusUnknown, StatusTag, StatusFacet, StatusDescription }; // Consecutive numbers
	string facet_id;
	string tag_id;
	string shortdesc;
	ostringstream longdesc;
	int tag_item = ItemNone;

	int status;
	string line;

	while (! in.eof() )
	{
		getline (in,line);

		if (line.size() == 0)
		{
			if (status != StatusNone)
			{
				if (tag_item == ItemFacet)
					AddFacet(facet_id, shortdesc, longdesc.str());
				else if (tag_item == ItemTag)
					AddTag(facet_id, tag_id, shortdesc, longdesc.str());
			}

			tag_id="";
			facet_id="";
			shortdesc="";
			tag_item = ItemNone;

			longdesc.clear();
			longdesc.str("");
			status = StatusNone;

			continue;
		}

		if (line[0] == '#')
		{
			continue;
		}
		else if (line[0] == ' ')
		{
			if (status == StatusDescription)
				longdesc << line << std::endl;
		}
		else
		{
			string label;
			char const *fieldseparator = ":";
			// extract label
			string::size_type sep = line.find_first_of(fieldseparator);
			if (sep != string::npos)
			{
				label = line.substr(0,sep);
				line = line.substr(sep+1);

				char const *whitespaces = " \t\r\n";
				// trim leading whitespaces from label
				string::size_type notwhite = label.find_first_not_of(whitespaces);
				label.erase(0,notwhite);
				// trim trailing whitespaces from label
				notwhite = label.find_last_not_of(whitespaces);
				label.erase(notwhite+1);

				// trim leading whitespaces from data
				notwhite = line.find_first_not_of(whitespaces);
				line.erase(0,notwhite);
				// trim trailing whitespaces from label
				notwhite = line.find_last_not_of(whitespaces);
				line.erase(notwhite+1);

				if (strcasecmp(label.c_str(),"tag") == 0)
				{
					string::size_type sep = line.find("::");
					if (sep != string::npos)
					{
						tag_item |= ItemTag;
						facet_id = line.substr(0, sep);
						tag_id = line.substr(sep+2);
						status = StatusTag;
					}
				}
				else if (strcasecmp(label.c_str(),"facet") == 0)
				{
					tag_item |= ItemFacet;
					status = StatusFacet;
					facet_id = line;
					tag_id = "";
				}
				else if (strcasecmp(label.c_str(),"description") == 0)
				{
					longdesc.clear();
					longdesc.str("");
					status = StatusDescription;
					shortdesc = line;
				}
				else
					status = StatusUnknown;
			}
		}
	}
	if (status != StatusNone)
	{
		if (tag_item == ItemFacet)
			AddFacet(facet_id, shortdesc, longdesc.str());
		else if (tag_item == ItemTag)
			AddTag(facet_id, tag_id, shortdesc, longdesc.str());
	}
	return true;
}

bool DebtagsMap::Write(std::ostream &out)
{
	bool ret = true;
	iterator p;
	for(p = begin(); p != end(); p++)
	{
		out << "Facet: " << p->first << std::endl;
		out << "Description: " << p->second.ShortDescription << std::endl;
		out << p->second.LongDescription << std::endl;
		ret &= p->second.Write(out);
	}
	return ret;
}

bool DebtagsMap::WritePot(std::ostream &out)
{
	bool ret = true;

	out << "# SOME DESCRIPTIVE TITLE." << std::endl;
	out << "# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER" << std::endl;
	out << "# This file is distributed under the same license as the PACKAGE package." << std::endl;
	out << "# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR." << std::endl;
	out << "#, fuzzy" << std::endl;
	out << "msgid \"\"" << std::endl;
	out << "msgstr \"\"" << std::endl;
	out << "\"Project-Id-Version: PACKAGE VERSION\\n\"" << std::endl;
	out << "\"Report-Msgid-Bugs-To: \\n\"" << std::endl;
	out << "\"POT-Creation-Date: 2009-05-14 01:57+0200\\n\"" << std::endl;
	out << "\"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\\n\"" << std::endl;
	out << "\"Last-Translator: FULL NAME <EMAIL@ADDRESS>\\n\"" << std::endl;
	out << "\"Language-Team: LANGUAGE <LL@li.org>\\n\"" << std::endl;
	out << "\"MIME-Version: 1.0\\n\"" << std::endl;
	out << "\"Content-Transfer-Encoding: 8bit\\n\"" << std::endl;
	out << std::endl;

	iterator p;
	for(p = begin(); p != end(); p++)
	{
		out << "# Facet: " << p->first << std::endl;
		out << "msgid \"" << p->second.ShortDescription << "\"" << std::endl;
		out << "msgstr \"\"" << std::endl;
//		out << p->second.LongDescription << std::endl;

		if (p->second.LongDescription.length() > 0)
		{
			out << std::endl << "# Long description for facet " << p->first << " (" << p->second.ShortDescription << ")"<< std::endl;
			out << "msgid \"";
			string desc = p->second.LongDescription;

			char const *lbreak = "\n";
			string::size_type notret = desc.find_last_not_of(lbreak);
			desc.erase(notret+1);

			if (desc[0] == ' ')
				desc.erase(0,1);

			string replace = "\n ";
			for (size_t j = 0; (j = desc.find( replace, j )) != string::npos ; j += 1)
				desc.replace( j, replace.length(), "\n" );
			replace = "\\";
			for (size_t j = 0; (j = desc.find( replace, j )) != string::npos ; j += 2)
				desc.replace( j, replace.length(), "\\\\" );
			replace = "\n";
			for (size_t j = 0; (j = desc.find( replace, j )) != string::npos ; j += 3)
				desc.replace( j, replace.length(), "\"\n\"" );
			out << desc << "\"" << std::endl;
			out << "msgstr \"\"" << std::endl;
		}

//		ret &= p->second.WritePot(out);
		out << std::endl;
	}
	return ret;
}

int main()
{
	DebtagsMap tags;
	ifstream file_in (DEBTAGS_VOCABULARY);
	if (file_in.is_open())
	{
		tags.Read(file_in);
		file_in.close();
	}
	else cout << "Unable to open file"; 


	ofstream file_out ("example.txt");
	if (file_out.is_open())
	{
		tags.Write(file_out);
		file_out.close();
	}
	else cout << "Unable to open file";

	tags.WritePot(cout);

	return 0;
}
