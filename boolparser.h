/*
    BoolParser.h - Boolean expression parser and syntax tree builder

    boolstuff - Disjunctive Normal Form boolean expression library
    Copyright (C) 2002-2005 Pierre Sarrazin <http://sarrazip.com/>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
    02111-1307, USA.
*/

#ifndef _GOFIND_BOOLPARSER_H
#define _GOFIND_BOOLPARSER_H

#include <boolstuff/BoolExpr.h>

#include <string>

/**
    Parser for a language of boolean expressions.
    The parse() method dynamically allocates a binary tree of nodes that
    represents the syntactic structure of a textual boolean expression.
*/
class BoolParser
{
public:

    /**  Error descriptor. */
    class Error
    {
    public:
        /** Possible error codes returned by the parser. */
        enum Code
        {
            GARBAGE_AT_END,
            RUNAWAY_PARENTHESIS,
            STRING_EXPECTED
        };

        /** Index (>=0) in the input string where the error was detected. */
        size_t index;
        /** Code that gives the type of the error */
        Code code;

        /**
            Initializes an error object with the given index and error.
        */
        Error(size_t i, Code c) : index(i), code(c) {}
    };


    /**
        Initializes the parser.
    */
    BoolParser();

    /**
        Destroys the parser and frees the associated resources.
    */
    ~BoolParser();

    /**
        Parses a textual boolean expression and creates a binary syntax tree.
        Dynamically allocates a tree of nodes that represents the
        syntactic structure of 'expr'.
        The returned tree must eventually be destroyed with operator delete.

        @param	expr	text of the boolean expression to parse
        @returns	the root of the created tree
        @throws	Error	describes a parsing error
    */
    boolstuff::BoolExpr<std::string> *parse(const std::string &expr) throw(Error);

    bool isStartingStringChar(char c) const;
    bool isStringChar(char c) const;

private:

    std::string curInput;
    size_t curIndex;

    // Implementation methods:
    boolstuff::BoolExpr<std::string> *parseExpr() throw(Error);
    boolstuff::BoolExpr<std::string> *parseTerm() throw(Error);
    boolstuff::BoolExpr<std::string> *parseFactor() throw(Error);
    boolstuff::BoolExpr<std::string> *parseAtom() throw(Error);
    boolstuff::BoolExpr<std::string> *parseString() throw(Error);

    bool atEnd();
    bool tokenSeen(const char *s);
    void skipToken(const char *s);
    void skipSpaces();

    // Forbidden operations:
    BoolParser(const BoolParser &);
    BoolParser &operator = (const BoolParser &);
};

#endif  // _GOFIND_BOOLPARSER_H
