/*
 * Copyright (C) 2007, 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "common.h"

#include "filter.h"
#include "taghandler.h"
#include "boolparser.h"

#ifdef UNIT_TEST
#include "CuTest.h"
#endif

#include <string>
#include <fstream>
#include <vector>

#include <string.h>
#include <stdio.h>
#include <errno.h>

#include <ept/debtags/tag.h>

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"

#define GREEN_MINIMUM 2

PackageFilter::PackageFilter() : list(NULL)
{
}

PackageFilter::~PackageFilter()
{
	DeleteList();
}

const char *const PackageFilter::TypeNames[16] = { NULL, "Unknown", "Recommended", "Safe", "Normal",
	"Warning", "Dangerous", "Intolerable", "Highlight", "Hidden", NULL };

using namespace std;

bool PackageFilter::Load(FILE *fd)
{
	fseek( fd, 0, SEEK_END);
	int fsize = ftell(fd);
	fseek( fd, 0, SEEK_SET);

	char *buffer = new char[fsize + 1];
	memset(buffer, 0, fsize + 1);
	fread(buffer, fsize, 1, fd);

	rapidxml::xml_document<> doc;
	try {
		doc.parse<0>(buffer);
	} catch (...) {
		std::cerr << "Error loading filter" << std::endl;
		return false;
	}

	//std::cout << doc;

	rapidxml::xml_node<> *data = doc.first_node("gofind");
	if (!data) return false;
	rapidxml::xml_attribute<> *version = data->first_attribute("version");
	if (!version) return false;
	if (atoi(version->value()) != 1) return false;

	Type type = PackageFilter::Unknown;
	int min_age = 0;

	for (rapidxml::xml_node<> *filter = data->first_node("filter");
			filter; filter = filter->next_sibling("filter"))
	{
		rapidxml::xml_attribute<> *target = filter->first_attribute("class");

		if (target)
		{
			std::cout << target->name() << " = " << target->value() << std::endl;

			if (strcasecmp(target->value(), "intolerable") == 0)       type = PackageFilter::Intolerable;
			else if (strcasecmp(target->value(), "dangerous") == 0)    type = PackageFilter::Dangerous;
			else if (strcasecmp(target->value(), "warning") == 0) type = PackageFilter::Warning;
			else if (strcasecmp(target->value(), "normal") == 0)  type = PackageFilter::Normal;
			else if (strcasecmp(target->value(), "safe") == 0)  type = PackageFilter::Safe;
			else if (strcasecmp(target->value(), "recommended") == 0)  type = PackageFilter::Recommended;
			else if (strcasecmp(target->value(), "highlight") == 0)   type = PackageFilter::Highlight;
			else if (strcasecmp(target->value(), "hidden") == 0) type = PackageFilter::Hidden;
			else type = PackageFilter::Unknown;
		}
		else type = PackageFilter::Unknown;

		rapidxml::xml_attribute<> *age = filter->first_attribute("age");

		if (age)
		{
			std::cout << age->name() << " = " << age->value() << std::endl;
			int age_num = atoi(age->value());
			if (min_age < age_num)
				min_age = age_num;
		}

		rapidxml::xml_attribute<> *tid = filter->first_attribute("id");
		if (tid)
			std::cout << tid->name() << " = " << tid->value() << std::endl;

		for (rapidxml::xml_node<> *rule = filter->first_node("rule");
			rule; rule = rule->next_sibling("rule"))
		{
			rapidxml::xml_attribute<> *condition = rule->first_attribute("condition");
			if (!condition) break;
			rapidxml::xml_attribute<> *rid = rule->first_attribute("id");
			if (rid)
				std::cout << rid->name() << " = " << rid->value() << std::endl;
				std::cout << type << std::endl;
			std::cout << condition->name() << " = " << condition->value() << std::endl;
			if (!AddLast(
				rid ? rid->value() : tid ? tid->value() : "?" ,
				type,
				min_age,
				condition->value()
			))
				std::cerr << "Error Adding '"<< condition->value() << "'" << std::endl;
		}
	}

	Print(std::cerr);
	std::cerr << std::endl;
	return true;
}

bool PackageFilter::Load(const char *filename)
{
	cout << "Loading filter: \"" << filename << "\"" << std::endl;

	FILE *fd = fopen(filename, "rb"); // Read-only mode
	if ( fd == NULL) {
		fprintf( stderr, "fopen failed, errno = %d (%s)\n", errno, strerror( errno));
		return false;
	}
	bool ret = Load(fd);
	fclose(fd);
	return ret;
}

/* Find out the color of a single tag */
int PackageFilter::TagValue(const Tag &tag)
{
	std::string name = tag.fullname();
	//std::string facet_name = tag.facet().name();
	//std::string tag_name = tag.name();

	Type type = PackageFilter::Unknown;

	// The order is important
	PackageFilter::ResultList *item = list;
	while (item != NULL) {
		if (tagdata.CheckTag(&item->positive, name))
		{
			//std::cout << "ITEM = " << item->name << std::endl;
			if (type < item->type)
				type = item->type;
		}
		item = item->next;
	}

	return type;
}

/* Find out the color of a set of tags */
int PackageFilter::TagsValue(const TagSet &tags)
{
	FilterTagHandler::Result t;
//	unsigned int i = 0;

//	tagdata.PrintAll(std::cerr);

	for (TagSet::const_iterator i = tags.begin(); i != tags.end(); ++i) {
		std::string name = i->fullname();
		//std::string facet_name = i->facet().name();
		//std::string tag_name = i->name();
//		std::cerr << "Add Tag: " << name << std::endl;
		tagdata.SetTagIfExists(&t, name);
	}

	Type type = PackageFilter::Unknown;

	// The order is important
	PackageFilter::ResultList *item = list;
	while (item != NULL) {
//		std::cerr << "Compare: ";
//		t.Print(std::cerr);
//		std::cerr << "With table value " << i++ << ": ";
//		item->Print(std::cerr);
//		std::cerr << std::endl;
		if (t.CompareAll(item->positive) && !t.CompareAny(item->negative))
		{
			//std::cout << "ITEM = " << item->name << std::endl;
			if (type < item->type)
				type = item->type;
		}
		item = item->next;
	}

	return type;
}


bool PackageFilter::AddLastAndHelper(ResultList *element, boolstuff::BoolExpr<std::string> *expr)
{
	bool success = true;

	//std::cout << "and  = " << expr << std::endl;

	switch (expr->getType())
	{
		case boolstuff::BoolExpr<std::string>::VALUE:
			tagdata.SetTag(&element->positive, expr->getValue());
			break;
		case boolstuff::BoolExpr<std::string>::NOT:
			if (expr->getRight() &&
				expr->getRight()->getType()==boolstuff::BoolExpr<std::string>::VALUE)
			{
				tagdata.SetTag(&element->negative, expr->getRight()->getValue());
			}
			else success=false;
			break;
		case boolstuff::BoolExpr<std::string>::AND:
			if (!success) break;
			if (expr->getLeft())
				success &= AddLastAndHelper(element, expr->getLeft());
			if (expr->getRight())
				success &= AddLastAndHelper(element, expr->getRight());
			break;
		case boolstuff::BoolExpr<std::string>::OR:
		default:
			success = false;
			break;
	}

	return success;
}

bool PackageFilter::AddLastOrHelper(const std::string str, Type type, int age, boolstuff::BoolExpr<std::string> *expr)
{
	bool success = true;

	//std::cout << "or  = " << expr << std::endl;

	ResultList *element = NULL;
	switch (expr->getType())
	{
		case boolstuff::BoolExpr<std::string>::VALUE:
		case boolstuff::BoolExpr<std::string>::NOT:
		case boolstuff::BoolExpr<std::string>::AND:
			if (!success) break;
			element = new ResultList(str, type, age);
			success &= AddLastAndHelper(element, expr);
			if (success) AddLast(element);
			else delete element;
			break;
		case boolstuff::BoolExpr<std::string>::OR:
			if (!success) break;
			if (expr->getLeft())
				success &= AddLastOrHelper(str, type, age, expr->getLeft());
			if (expr->getRight())
				success &= AddLastOrHelper(str, type, age, expr->getRight());
			break;
		default:
			success = false;
			break;
	}

	return success;
}

bool PackageFilter::AddLast(const std::string str, Type type, int age, const std::string &bool_expr)
{
	bool success = true;
	BoolParser parser;
	try
	{
		boolstuff::BoolExpr<std::string> *expr = parser.parse(bool_expr);
		assert(expr != NULL);

		expr = boolstuff::BoolExpr<std::string>::getDisjunctiveNormalForm(expr);
		if (expr == NULL)
		{
			std::cout << "result too large" << std::endl;
			success = false;
		}
		else
		{
			//std::cout << "expr = " << expr << std::endl;
			success &= AddLastOrHelper(str, type, age, expr);
		}

		delete expr;
	}
	catch (BoolParser::Error &err)
	{
		std::cout << "? column " << err.index + 1
			<< " : ";
		success = false;
		switch (err.code)
		{
			case BoolParser::Error::GARBAGE_AT_END:
				std::cout << "garbage at end";
				break;
			case BoolParser::Error::RUNAWAY_PARENTHESIS:
				std::cout << "runaway parenthesis";
				break;
			case BoolParser::Error::STRING_EXPECTED:
				std::cout << "string expected";
				ResultList *element = new ResultList(str, type, age);
				AddLast(element);
				success = true;
				break;
		}
		std::cout << std::endl;
	}
	
	if (success)
	{
	}

	return success;
}

#ifdef UNIT_TEST
TEST_FUNCTION TestCuPackageFilter(CuTest* tc)
{
}
#endif
