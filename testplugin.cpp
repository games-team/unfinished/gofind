/*
 * Copyright (C) 1995  Jeff Koftinoff <jeffk@jdkoftinoff.com>
 * Copyright (C) 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * http://www.jdkoftinoff.com/main/Articles/Linux_Articles/ELF_Plugins/ 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "common.h"
#include "testplugin.h"

class MyTestPlugIn : public TestPlugIn
{
 public:
	MyTestPlugIn(TestPlugInFactory *f) : TestPlugIn(f)
	{
	}

	virtual ~MyTestPlugIn()
	{
	}

	virtual bool GetTrue();
	virtual bool GetFalse();
	virtual bool GetSame(bool p);
	virtual bool GetOther(bool p);
};

bool MyTestPlugIn::GetTrue()
{
	return true;
}

bool MyTestPlugIn::GetFalse()
{
	return false;
}

bool MyTestPlugIn::GetSame(bool p)
{
	return p;
}

bool MyTestPlugIn::GetOther(bool p)
{
	return !p;
}

class MyTestPlugInFactory : public TestPlugInFactory
{
 public:
	MyTestPlugInFactory()
	{
	}

	~MyTestPlugInFactory()
	{
	}

	virtual TestPlugIn * CreatePlugIn()
	{
		return new MyTestPlugIn(this);
	}
};

//
// The "C" linkage factory0() function creates the PlugIn Factory
// class for this library
//

extern "C" void * factory0( void )
{
	return new MyTestPlugInFactory;
}
