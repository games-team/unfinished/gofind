#! /usr/bin/env lua

-- The simplest possible Lua-Gtk application.

require "gtk"

local i=0; while arg[i] do print(i, arg[i]); i=i+1 end

win = gtk.window_new(gtk.GTK_WINDOW_TOPLEVEL)
win:connect('destroy', function() gtk.main_quit() end)
win:set_title("Demo Program")
win:show()
gtk.main()

