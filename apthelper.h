/*
 * Copyright (C) 2009  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _games_apthelper_h
#define _games_apthelper_h

#include <cstdlib>
#include <string>

class pkgCache;
class pkgSourceList;
class MMap;

class AptHelper
{
public:
	struct PackageInfo {
		std::string Name;
		std::string ShortDescription;
		std::string LongDescription;
		std::string LanguageCode;
		void Clean()
		{
			Name="";
			ShortDescription="";
			LongDescription="";
			LanguageCode="";
		}
	};

	AptHelper(int argc,const char *argv[]);
	~AptHelper();

	inline bool IsInit() { return Init; }
	bool GetPackageInfo(const std::string &package_name, PackageInfo *info);
	bool ShowPackage(const std::string &package_name);

	static int LocalityCompare(const void *a, const void *b);

private:
	static bool Init;

	pkgCache *GCache;
	pkgSourceList *SrcList;
	MMap *Map;
};

#endif // _games_apthelper_h
