/*
 * Copyright (C) 2008-2009  Miriam Ruiz <little_miry@yahoo.es>
 *
 * http://www.jdkoftinoff.com/main/Articles/Linux_Articles/ELF_Plugins/ 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "i18n.h"

#include "pkgdata.h"
#include "common.h"
#include "utf8/html.h"

#include "Environment.h"
#include "GamesOptions.h"
#include "pkgdata.h"
#include "apthelper.h"

#include <ept/apt/packagerecord.h>
#include <wibble/regexp.h>
#include <wibble/string.h>
#include <xapian.h>

#include <iostream>
#include <string>
#include <cmath>
#include <vector>

#include <string.h>

PackageData::PackageData(int argc,const char *argv[]) : apt_helper(NULL)
{
	apt_helper = new AptHelper(argc, argv);
}

PackageData::~PackageData()
{
	if (apt_helper) delete apt_helper;
}

bool PackageData::AppTemplate(const char *id)
{
	if (strcmp(id, "learn") == 0) {
		mainFacet = "field";
		secondaryFacet = "interface";
		globalFilter = Xapian::Query(Xapian::Query::OP_AND,
			Xapian::Query(Xapian::Query::OP_OR,
				Xapian::Query("XTrole::documentation"),
				Xapian::Query("XTrole::program")),
			Xapian::Query("XTuse::learning"));
	} else if (strcmp(id, "admin") == 0) {
		mainFacet = "admin";
		secondaryFacet = "interface";
		globalFilter = Xapian::Query(Xapian::Query::OP_OR,
				Xapian::Query("XTrole::documentation"),
				Xapian::Query("XTrole::program"));
	} else if (strcmp(id, "net") == 0) {
		mainFacet = "network";
		secondaryFacet = "interface";
		globalFilter = Xapian::Query(Xapian::Query::OP_OR,
				Xapian::Query("XTrole::documentation"),
				Xapian::Query("XTrole::program"));
	} else if (strcmp(id, "office") == 0) {
		mainFacet = "office";
		secondaryFacet = "interface";
		globalFilter = Xapian::Query(Xapian::Query::OP_OR,
				Xapian::Query("XTrole::documentation"),
				Xapian::Query("XTrole::program"));
	} else if (strcmp(id, "safe") == 0) {
		mainFacet = "security";
		secondaryFacet = "interface";
		globalFilter = Xapian::Query(Xapian::Query::OP_OR,
				Xapian::Query("XTrole::documentation"),
				Xapian::Query("XTrole::program"));
	} else if (strcmp(id, "web") == 0) {
		mainFacet = "web";
		secondaryFacet = "interface";
		globalFilter = Xapian::Query(Xapian::Query::OP_OR,
				Xapian::Query("XTrole::documentation"),
				Xapian::Query("XTrole::program"));
	} else if (strcmp(id, "play") == 0) {
		mainFacet = "game";
		secondaryFacet = "interface";
		globalFilter = Xapian::Query("XTrole::program");
	} else {
		return false;
	}

	return true;
}

static const char legalchars[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@_?+-,.~/%&=:*#";

void PackageData::AddTextAsHTML(const std::string &in_text, std::string &out_html)
{
	std::ostringstream os;
	utf8::html::QuoteHTML(os, in_text.c_str());
	os << std::ends;
	os.flush();
	const std::string & tempString(os.str());
	const char *text = tempString.c_str();

	const char *src = text;
	//out_html.erase();
	while (*src)
	{
		if (!strncasecmp("http:", src, 5) || !strncasecmp("mailto:", src, 7) ||
			!strncasecmp("ftp:", src, 4) || !strncasecmp("https:", src, 6))
		{
			std::string uri, uri_text;
			for (; *src && (strchr(legalchars, *src) || (*src & 128)); src++)
			{
				uri += *src;
				uri_text += *src;
				if (*src == '/')
					uri_text += "<wbr>";
			}
			if (strchr(".?!", src[-1])) {  // Probably the end of a sentence
				--src;
				uri.resize(uri.size() - 1);
				uri_text.resize(uri_text.size() - 1);
			}
			out_html += "<a href=\"";
			out_html += uri;
			out_html += "\">";
			out_html += uri_text;
			out_html += "</a>";
		}
		else if (!strncasecmp(" .\n", src, 3))
		{
			out_html += "<br />&nbsp;<br />";
			src += 2;
		}
		else if (*src <= ' ')
		{
			out_html += " ";
			src++;
		}
		else
		{
			switch(*src)
			{
				case '<': out_html += "&lt;"; break;
				case '>': out_html += "&gt;"; break;
				default:
				out_html += *src;
			}
			src++;
		}
	}
}

bool PackageData::GetPkgShortDesc(const std::string &pkgname, std::string &shortdesc)
{
	if (!apt_helper || !apt_helper->IsInit()) return false;
	AptHelper::PackageInfo info;
	if (!apt_helper->GetPackageInfo(pkgname, &info))
		return false;
	shortdesc = info.ShortDescription;
	return true;
}

bool PackageData::GetPkgHTMLShortDesc(const std::string &pkgname, std::string &html_short)
{
	std::string shdesc;
	if (!GetPkgShortDesc(pkgname, shdesc))
		return false;
	html_short.erase();
	AddTextAsHTML(shdesc, html_short);
	return true;
}

bool PackageData::GetPkgLatin1ShortDesc(const std::string &pkgname, std::string &desc)
{
	std::string shdesc;
	if (!GetPkgShortDesc(pkgname, shdesc))
		return false;
	std::ostringstream os;
	utf8::html::UTF8ToLatin1(os, shdesc.c_str());
	os << std::ends;
	os.flush();
	desc = os.str();
	return true;
}

bool PackageData::GetPkgHTMLDesc(const std::string &pkgname, std::string &html_short, std::string &html_long)
{
	if (!apt_helper || !apt_helper->IsInit()) return false;

	AptHelper::PackageInfo info;
	if (!apt_helper->GetPackageInfo(pkgname, &info))
		return false;

//	std::cout << "Description"
//		<< ( (strcmp(info.LanguageCode.c_str(),"") != 0) ? "-" : "" ) << info.LanguageCode
//		<< ": " << info.ShortDescription << std::endl << info.LongDescription << std::endl;

	html_short.erase();
	AddTextAsHTML(info.ShortDescription, html_short);

	html_long.erase();
	AddTextAsHTML(info.LongDescription, html_long);

	return true;
}

char *PackageData::TagString(const Tag& tag)
{
	static std::map<std::string, char*> table;
	std::map<std::string, char*>::iterator i = table.find(tag.fullname());
	if (i == table.end())
	{
		std::pair< std::map<std::string, char*>::iterator, bool > tmp =
			table.insert(make_pair(tag.fullname(), strdup(tag.fullname().c_str())));
		i = tmp.first;
	}
	return i->second;
}

char *PackageData::PackageString(const std::string& name)
{
	static std::map<std::string, char*> table;
	std::map<std::string, char*>::iterator i = table.find(name);
	if (i == table.end())
	{
		std::pair< std::map<std::string, char*>::iterator, bool > tmp =
			table.insert(make_pair(name, strdup(name.c_str())));
		i = tmp.first;
	}
	return i->second;
}

void PackageData::PrintResults()
{
	const std::vector<Result>& packages = results();
	for (std::vector<Result>::const_iterator i = packages.begin();
			i != packages.end(); ++i)
	{
		ept::apt::PackageRecord pkg = apt().rawRecord(i->name);
		std::cerr << "PKG " << pkg.package() << " - " << pkg.shortDescription() << std::endl;
	}

	const std::set<Tag>& ttags = types();
	for (std::set<Tag>::const_iterator i = ttags.begin();
			i != ttags.end(); ++i)
	{
		std::cerr << "TTAG " << i->fullname() << std::endl;
	}

	const std::set<Tag>& ftags = interfaces();
	for (std::set<Tag>::const_iterator i = ftags.begin();
			i != ftags.end(); ++i)
	{
		std::cerr << "ITAG " << i->fullname() << std::endl;
	}
}
