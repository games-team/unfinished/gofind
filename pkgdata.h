/*
 * Copyright (C) 2008-2009  Miriam Ruiz <little_miry@yahoo.es>
 *
 * http://www.jdkoftinoff.com/main/Articles/Linux_Articles/ELF_Plugins/ 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _GOFIND_PACKAGEDATA_H
#define _GOFIND_PACKAGEDATA_H

#include "dll.h"
#include "Engine.h"
#include "filter.h"

#include <ept/debtags/tag.h>
#include <string>

using namespace ept;

class AptHelper;

class PackageData : public Engine
{
 public:
	PackageData(int argc,const char *argv[]);
	~PackageData();

	typedef ept::debtags::Tag Tag;
	typedef std::set<Tag> TagSet;

	bool AppTemplate(const char *id);

	bool GetPkgShortDesc(const std::string &pkgname, std::string &shortdesc);
	bool GetPkgHTMLShortDesc(const std::string &pkgname, std::string &html_short);
	bool GetPkgLatin1ShortDesc(const std::string &pkgname, std::string &desc);
	bool GetPkgHTMLDesc(const std::string &pkgname, std::string &html_short, std::string &html_long);

	char *TagString(const Tag& tag);
	char *PackageString(const std::string& name);
	void PrintResults();

	static void AddTextAsHTML(const std::string &in_text, std::string &out_html);

	PackageFilter &GetPackageFilter()
	{
		return pkgfilter;
	}

protected:
	PackageFilter pkgfilter;
	AptHelper *apt_helper;
};

#endif // _GOFIND_PACKAGEDATA_H
