/*
 * debtags - Implement package tags support for Debian
 *
 * Copyright (C) 2007  Enrico Zini <enrico@debian.org>
 * Copyright (C) 2007-2009  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "i18n.h"
#include "common.h"
#include "guiplugin.h"

#include "Environment.h"
#include "GamesOptions.h"
#include "pkgdata.h"

#include <ept/apt/packagerecord.h>
#include <wibble/regexp.h>
#include <wibble/string.h>
#include <xapian.h>

#include <iostream>
#include <cmath>

#include <basedir.h>
#include <basedir_fs.h>

#include <string.h>

namespace std {

	template<typename TAG, typename _Traits>
	basic_ostream<char, _Traits>& operator<<(basic_ostream<char, _Traits>& out, const std::set<TAG>& tags)
	{
		for (typename std::set<TAG>::const_iterator i = tags.begin();
				i != tags.end(); i++)
			if (i == tags.begin())
				out << i->fullname();
			else
				out << ", " << i->fullname();
		return out;
	}

	template<typename TAG, typename _Traits>
	basic_ostream<char, _Traits>& operator<<(basic_ostream<char, _Traits>& out, const wibble::Singleton<TAG>& tags)
	{
		out << *tags.begin();
		return out;
	}

	template<typename TAG, typename _Traits>
	basic_ostream<char, _Traits>& operator<<(basic_ostream<char, _Traits>& out, const wibble::Empty<TAG>&)
	{
		return out;
	}

}

using namespace std;
using namespace ept;
using namespace ept::debtags;
using namespace ept::apt;
using namespace ept::textsearch;

#ifndef UNIT_TEST
int main(int argc, const char* argv[])
{
	wibble::commandline::GamesOptions opts;

	try {
		// Install the handler for unexpected exceptions
		wibble::exception::InstallUnexpected installUnexpected;

		if (opts.parse(argc, argv))
			return 0;

		if (opts.out_verbose->boolValue())
			::Environment::get().verbose(true);

		if (opts.out_debug->boolValue())
			::Environment::get().debug(true);

		std::string plugin_file = opts.gui->isSet() ? std::string("./gui_")  + opts.gui->stringValue() + std::string(".so") : "./gui_fltk.so";
		GUIPlugIn gui_plugin( plugin_file.c_str() );

		if( gui_plugin.LastError())
		{
			std::cout << _("Could not load GUI plugin.") << std::endl;
			return -1;
		}

		gui_plugin.Init(argc, argv);
		gui_plugin.Comment(_("Starting system"));

		PackageData pkgdata(argc, argv);

		if (wibble::str::endsWith(argv[0], "learn") || opts.gowhere->stringValue() == "learn")
			pkgdata.AppTemplate("learn");
		else if (wibble::str::endsWith(argv[0], "admin") || opts.gowhere->stringValue() == "admin")
			pkgdata.AppTemplate("admin");
		else if (wibble::str::endsWith(argv[0], "net") || opts.gowhere->stringValue() == "net")
			pkgdata.AppTemplate("net");
		else if (wibble::str::endsWith(argv[0], "office") || opts.gowhere->stringValue() == "office")
			pkgdata.AppTemplate("office");
		else if (wibble::str::endsWith(argv[0], "safe") || opts.gowhere->stringValue() == "safe")
			pkgdata.AppTemplate("safe");
		else if (wibble::str::endsWith(argv[0], "web") || opts.gowhere->stringValue() == "web")
			pkgdata.AppTemplate("web");
		else
			pkgdata.AppTemplate("play");

		if (opts.mainFacet->isSet())
			pkgdata.mainFacet = opts.mainFacet->stringValue();

		if (opts.secondaryFacet->isSet())
			pkgdata.secondaryFacet = opts.secondaryFacet->stringValue();

		if (opts.ftags->isSet())
		{
			Xapian::Query fquery;
			wibble::Splitter tags(", *", REG_EXTENDED);
			bool first = true;
			for (wibble::Splitter::const_iterator i = tags.begin(opts.ftags->stringValue());
					i != tags.end(); ++i)
			{
				if (first)
				{
					fquery = Xapian::Query("XT"+*i);
					first = false;
				}
				else
					fquery = Xapian::Query(Xapian::Query::OP_AND, fquery, Xapian::Query("XT"+*i));
			}
			pkgdata.globalFilter = fquery;
		}

		if (opts.filter->isSet())
		{
			pkgdata.GetPackageFilter().Load(opts.filter->stringValue().c_str());
		}
		else
		{
			pkgdata.GetPackageFilter().Clean();
			xdgHandle xdg_handle;
			if (xdgInitHandle(&xdg_handle))
			{
				FILE *fd=xdgConfigOpen("gofind/filter.cfg", "rb", &xdg_handle);
				if (fd)
				{
					pkgdata.GetPackageFilter().Load(fd);
					fclose(fd);
				}
				xdgWipeHandle(&xdg_handle);
			}
			else std::cerr << "Error initializing XDG Handle" << std::endl;
		}

		/*
		cerr << " *** Initial:" << endl;
		printResults(pkgdata);

		pkgdata.setTypeFilter(pkgdata.voc().tagByName("game::arcade"));
		cerr << " *** Arcades:" << endl;
		printResults(pkgdata);

		pkgdata.setInterfaceFilter(pkgdata.voc().tagByName("interface::x11"));
		cerr << " *** X11 Arcades:" << endl;
		printResults(pkgdata);

		pkgdata.setInstalledFilter(pkgdata::INSTALLED);
		cerr << " *** Installed X11 Arcades:" << endl;
		printResults(pkgdata);
		*/

		gui_plugin.Go(pkgdata);

		return 0;
	} catch (wibble::exception::BadOption& e) {
		cerr << e.desc() << endl;
		opts.outputHelp(cerr);
		return 1;
	} catch (std::exception& e) {
		cerr << e.what() << endl;
		return 1;
	} catch (Xapian::InvalidArgumentError& e) {
		cerr << "Xapian " << e.get_type() << ": " << e.get_msg();
		if (!e.get_context().empty())
			cerr << ". Context: " << e.get_context();
		cerr << endl;
		cerr << endl;
		cerr << "It could be that your Apt Xapian index is missing: you can create it by running ept-cache reindex as root." << endl;
	} catch (Xapian::DatabaseVersionError& e) {
		cerr << "Xapian " << e.get_type() << ": " << e.get_msg();
		if (!e.get_context().empty())
			cerr << ". Context: " << e.get_context();
		cerr << endl;
		cerr << endl;
		cerr << "Please recreate the database by removing /var/lib/apt-xapian and running ept-cache reindex as root." << endl;
	} catch (Xapian::Error& e) {
		cerr << "Xapian " << e.get_type() << ": " << e.get_msg();
		if (!e.get_context().empty())
			cerr << ". Context: " << e.get_context();
		cerr << endl;
		return 1;
	}
}
#endif

#include <ept/debtags/debtags.tcc>

// vim:set ts=4 sw=4:
