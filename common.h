/*
 * Copyright (C) 2007  Enrico Zini <enrico@debian.org>
 * Copyright (C) 2007, 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _GOFIND_COMMON_H
#define _GOFIND_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define USE_UTF8

#ifdef HAVE_CONFIG_H
#include <config.h>
#define APPNAME PACKAGE_NAME
#else
//#warning No config.h found: using fallback values
#define APPNAME __FILE__
#define PACKAGE_VERSION "unknown"
#define VERSION "0.0"
#endif

#ifdef UNIT_TEST
#include "CuTest.h"
#endif

#ifndef DATADIR
#define DATADIR "/usr/share/goplay"
#endif

#ifndef HTMLDIR
#define HTMLDIR DATADIR "/html"
#endif

#ifndef FILE_NO_SCREENSHOT
#define FILE_NO_SCREENSHOT DATADIR "/no_screenshot.png"
#endif

#ifndef FILE_STARS_ON
#define FILE_STARS_ON DATADIR "/stars_on.png"
#endif

#ifndef FILE_STARS_OFF
#define FILE_STARS_OFF DATADIR "/stars_off.png"
#endif

#ifndef FILE_PEOPLE_ON
#define FILE_PEOPLE_ON DATADIR "/people_on.png"
#endif

#ifndef FILE_PEOPLE_OFF
#define FILE_PEOPLE_OFF DATADIR "/people_off.png"
#endif

#ifndef THUMBNAILSDIR
#define THUMBNAILSDIR "/usr/share/games/thumbnails"
#endif

#include <stdlib.h>
#include <stdio.h>

#define ERROR_PRINTF(...) { fprintf(stderr, "%s:%s:%u Error: ", __FILE__, __PRETTY_FUNCTION__, __LINE__); fprintf(stderr,__VA_ARGS__); fflush(stderr); }

#ifdef __cplusplus
}
#endif

#endif // _GOFIND_COMMON_H
