/*
 * Copyright (C) 1995  Jeff Koftinoff <jeffk@jdkoftinoff.com>
 * Copyright (C) 2008  Miriam Ruiz <little_miry@yahoo.es>
 *
 * http://www.jdkoftinoff.com/main/Articles/Linux_Articles/ELF_Plugins/ 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _GOFIND_TESTPLUGIN_H
#define _GOFIND_TESTPLUGIN_H

class TestPlugInFactory;

class TestPlugIn
{
 public:
	TestPlugIn(TestPlugInFactory *f) : factory(f)
	{
	}

	virtual ~TestPlugIn()
	{
	}

	virtual bool GetTrue() = 0;
	virtual bool GetFalse() = 0;
	virtual bool GetSame(bool p) = 0;
	virtual bool GetOther(bool p) = 0;
 
 protected:
	 TestPlugInFactory *factory;
};

class TestPlugInFactory
{
 public:
	TestPlugInFactory() 
	{
	}
	
	virtual ~TestPlugInFactory()
	{
	}
	
	virtual TestPlugIn * CreatePlugIn() = 0;
};

#endif // _GOFIND_TESTPLUGIN_H
